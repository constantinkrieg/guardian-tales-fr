export let guilds =[
    {
        id:"0",
        alt:"",
        title: "",
        group:"",
        server: "EU - Monde 1",
        description: "",  
        discord:"",
        display: "hidden",
        dmg: ""
    },
    {
        id:"1",
        alt:"impe",
        title: "Impératrice",
        group:"Alliance Tarot",
        server: "EU - Monde 1",
        description: "Impératrice est une guilde compétitive, mais pas prise de tête.\
        Son crédo : avoir le plus d'adjoints possible pour que chacun en fasse le moins possible.\
        Notre emoji totem : 🦥. Tous les membres sont invités à participer à l'organisation de la vie de la guilde, selon le temps dont ils disposent. \
        Trois entrées par jour obligatoires, en respectant les assignations de boss, participation au discord obligatoire, attaques avant minuit dans la mesure du possible.",  
        discord:"https://discord.gg/7pKC95rBFN",
        display: "true",
        dmg: "+650M"
    },
    {
        id:"2",
        alt:"temp",
        title: "Tempérance",
        group:"Alliance Tarot",
        server: "EU - Monde 1",
        description: "Si vous cherchez une guilde try hard mais pas trop, Tempérance est faite pour vous 😉 \
        Nous aidons énormément nos membres à progresser et faisons tout pour que leurs teams soient le plus opti possible dans les raids 💪\
        Nous ne faisons pas de reset pixel, mais tout le monde fait ses attaques avec efficacité dans le respect de chacun 🙂 Nous demandons :\
        <ul>\
        <li>3 Attaques + reports obligatoires</li>\
        <li>Respectez les assignations des teams et des Boss</li>\
        <li>Respectez l'horaire de notre règlement</li>\
        <li>Discord obligatoire</li>\
        </ul>Alors n'hésitez pas, demander un accès au Bar des Tempés, on vous accueillera avec grand plaisir 😉 Tempérance, une expérience tout en puissance ♫",  
        discord:"https://discord.gg/7pKC95rBFN",
        display: "true",
        dmg: "+600M"
    },
    {
        id:"3",
        alt:"sole",
        title: "Soleil",
        group:"Alliance Tarot",
        server: "EU - Monde 1",
        description: "Guilde Raid sans règles hormis de faire ses trois attaques par raid, si votre score n'est pas assez élevé vous pourrez être amenés à aller chez Force. Si vous faites de bons scores avec assiduité on vous demandera de monter chez Impératrice ou Tempérance (selon places disponibles)",  
        discord:"https://discord.gg/7pKC95rBFN",
        display: "true",
        dmg: "+350M"
    },
    {
        id:"4",
        alt:"forc",
        title: "Force",
        group:"Alliance Tarot",
        server: "EU - Monde 1",
        description: "Guilde Raid où le raid est peu encadré, juste essayez de faire vos attaques et de contribuer au score de guilde. Débutants acceptés.",  
        discord:"https://discord.gg/7pKC95rBFN",
        display: "true",
        dmg: ""
    },
    {
        id:"5",
        alt:"juge",
        title: "Jugement",
        group:"Alliance Tarot",
        server: "EU - Monde 1",
        description: "Guilde sans raid pour ceux qui veulent juste faire du pvp ou parfois souffler un peu et sortir de la compétition. Les membres sont kicks lorsqu'on a besoin de place et qu'ils ne se sont pas connectés sur les jeu depuis un certain temps",  
        discord:"https://discord.gg/7pKC95rBFN",
        display: "hidden",
        dmg: ""
    },
    {
        id:"6",
        alt:"styx",
        title: "Styx",
        group:"Cha0s Invaders",
        server: "EU - Monde 1",
        description: "",  
        discord:"https://discord.gg/cf6WXxNX2H",
        display: "true",
        dmg: "+350M"
    },
    {
        id:"7",
        alt:"chro",
        title: "Chronos",
        group:"Cha0s Invaders",
        server: "EU - Monde 1",
        description: "",  
        discord:"https://discord.gg/cf6WXxNX2H",
        display: "true",
        dmg: "+200M"
    },
    {
        id:"8",
        alt:"prom",
        title: "PrOminence",
        group:"Cha0s Invaders",
        server: "EU - Monde 1",
        description: "",  
        discord:"https://discord.gg/cf6WXxNX2H",
        display: "true",
        dmg: "+100M"
    },
    {
        id:"9",
        alt:"epsi",
        title: "EspilOn",
        group:"Cha0s Invaders",
        server: "EU - Monde 1",
        description: "Spécial débutants & Chill",  
        discord:"https://discord.gg/cf6WXxNX2H",
        display: "true",
        dmg: ""
    },
    {
        id:"10",
        alt:"symp",
        title: "Symphonia",
        group:"",
        server: "EU - Monde 1",
        description: "Symphonia est une guilde qui a bientôt 1 an et demi. Bien installés dans le top 100, on compte bien progresser toujours + ! <br/>On ne demande que 15 attaques minimum par semaine de raid, puis le score indiqué 🙂",  
        discord:"https://discord.gg/vhQMfxcnFJ",
        display: "true",
        dmg: "+100M"
    },
    {
        id:"11",
        alt:"",
        title: "FrenchGold",
        group:"",
        server: "EU - Monde 1",
        description: "Vous avez envie de rejoindre une guilde à l'ambiance familiale ? 🥳🥳🥳<br/>Une guilde HL où on ne se prend pas la tête ?<br/>Où on discute avec bienveillance sur tous les sujets (chan NSFW disponible sur demande 🍆) et où on prendra soin d'optimiser vos teams sur tous les modes du jeu 🔥🔥🔥?<br/>Frenchgold est fait pour vous 🫂",  
        discord:"https://discord.gg/2BxbCJwbxm",
        display: "true",
        dmg: "+150M"
    },
]