import { Callout } from 'nextra-theme-docs'
import { useState } from "react";
import ReactPlayer from 'react-player'
import { Tooltip } from 'react-tooltip'

export default function Team({ onVideoPlay, heroes, weapons, access, cards, dmg, video, videoSpaceship, relic, pattern, warning, chain1, chain2, chain3, chain4, chain5, chainSpaceship1, chainSpaceship2, chainSpaceship3, chainSpaceship4, chainSpaceship5 }) {
  console.log(onVideoPlay, typeof onVideoPlay);

  const [moreInfos, setMoreInfos] = useState(false);
  const [displayVideo, setDisplayVideo] = useState(false);
  const [displayVideoSpaceship, setDisplayVideoSpaceship] = useState(false);
  
  const [tab, setTab] = useState(0);

  const handleVideoPlay = (videoUrl) => {
    onVideoPlay(videoUrl); // This directly calls the passed function
  };

  return (
    <>    
    <div className={`box small teams ${moreInfos ? "collapsed" : ""}`} onClick={() => setMoreInfos(!moreInfos)}>
      <div className="infos">
        <div className="build">
          <div className="display-team">
            <div className="heroes">
              <div>
                <img alt="hero0" src={`/static/heroes/${heroes[0]}.png`} />      
              </div>

              <div>
                <img alt="hero1" src={`/static/heroes/${heroes[1]}.png`} />     
              </div>

              <div>
                <img alt="hero2" src={`/static/heroes/${heroes[2]}.png`} />             
              </div>

              <div>
                <img alt="hero3" src={`/static/heroes/${heroes[3]}.png`} />              
              </div>     
              <div className={`inner-box m-0`}>
                <img alt="relic" src={`/static/relics/${relic}.png`} />              
              </div>           
            </div>
            <div className="inner-box damage">
              <p><small className="o-30">DMG - Boss LV95</small></p>
              <Tooltip id="hard" />
              <Tooltip id="decrease" />
              {dmg.length > 0 &&
              <p className="dmg">
                {`≈${dmg}M`}
                {warning.length > 0 && warning.includes('💀') ? (
                  <span data-tooltip-id="hard" data-tooltip-content="Difficile à survivre" data-tooltip-place="top">💀</span>
                  ) : warning.length > 0 && warning.includes('🔻') ? (
                    <span data-tooltip-id="decrease" data-tooltip-content="Optimal avant Tour 28" data-tooltip-place="top"> 🔻</span>
                  ) : (
                  <span> 🟢</span>
                )}
              </p>
              }
              {dmg.length === 0 &&
              <p className="dmg">
                N/A        
                {warning.length > 0 && warning.includes('💀') ? (
                  <span data-tooltip-id="hard" data-tooltip-content="Difficile à survivre" data-tooltip-place="top">💀</span>
                  ) : warning.length > 0 && warning.includes('🔻') ? (
                    <span data-tooltip-id="decrease" data-tooltip-content="Optimal avant Tour 28" data-tooltip-place="top"> 🔻</span>
                  ) : (
                  <span> 🟢</span>
                )}
              </p>
              }         
            </div>
          </div>
          <div className="outline-section w-100 m-1 fd-column p-1">
          <div className={`links`}>

          <Tooltip id="preset" />
            <a className={`btn hex ${tab === 1 ? 'active' : ''}`} onClick={() => setTab(1)} data-tooltip-id="preset" data-tooltip-content="Préréglage" data-tooltip-place="top">
              <img width="50" alt="display" src="/static/icons/preset.svg"/>
            </a>
         
            <Tooltip id="chain" />
            <a className={`btn hex ${chain1.length > 0 ? '' : 'hidden'} ${tab === 2 ? 'active' : ''}`} onClick={() => setTab(2)} data-tooltip-id="chain" data-tooltip-content="Chaînes" data-tooltip-place="top">
              <img width="50" alt="display" src="/static/icons/chain.svg"/>
            </a>

            <Tooltip id="info" />
            <a className={`btn hex ${warning.length > 0 ? '' : 'hidden'} ${tab === 3 ? 'active' : ''}`}  onClick={() => setTab(3)} data-tooltip-id="info" data-tooltip-content="Informations" data-tooltip-place="top">
              <img width="50" alt="display" src="/static/icons/info.svg"/>
            </a>

            <Tooltip id="video" />
            {pattern === "1" &&
            <a className={`btn hex ${video.length > 0 ? "" : "hidden"}`} onClick={() => onVideoPlay(video)} data-tooltip-id="video" data-tooltip-content="Vidéo" data-tooltip-place="top" > 
            <img width="50" alt="display" src="/static/icons/video.svg" /> 
            </a>
            }

            <Tooltip id="video1" />
            {pattern !== "1" &&
            <a className={`btn hex ${video.length > 0 ? "" : "hidden"}`} onClick={() => onVideoPlay(video)} data-tooltip-id="video" data-tooltip-content="Vidéo" data-tooltip-place="top" > 
            <img width="50" alt="display" src="/static/icons/video-1.svg" /> 
            </a>
            }

            <Tooltip id="video2" />
            {pattern !== "1" &&
            <a className={`btn hex ${video.length > 0 ? "" : "hidden"}`} onClick={() => onVideoPlay(video)} data-tooltip-id="video" data-tooltip-content="Vidéo" data-tooltip-place="top" > 
            <img width="50" alt="display" src="/static/icons/video-2.svg" /> 
            </a>
            }  
          </div>     
          {tab == 1 &&
        <div className="box w-100 my-1 p-1 tab">
        <b className="subtitle"><u>Préréglage</u></b>
          <div className="d-flex">
            <div className={`inner-box m-1`}>
              <img alt="hero" src={`/static/heroes/${heroes[0]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="weapon" src={`/static/weapons/${weapons[0]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="access" src={`/static/access/${access[0]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/cards/${cards[0]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/relics/${relic}.png`} />
            </div>
          </div>
          <div className="d-flex">
            <div className={`inner-box m-1`}>
              <img alt="hero" src={`/static/heroes/${heroes[1]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="weapon" src={`/static/weapons/${weapons[1]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="access" src={`/static/access/${access[1]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/cards/${cards[1]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/relics/${relic}.png`} />
            </div>
          </div>
          <div className="d-flex">
            <div className={`inner-box m-1`}>
              <img alt="hero" src={`/static/heroes/${heroes[2]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="weapon" src={`/static/weapons/${weapons[2]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="access" src={`/static/access/${access[2]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/cards/${cards[2]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/relics/${relic}.png`} />
            </div>
          </div>
          <div className="d-flex">
            <div className={`inner-box m-1`}>
              <img alt="hero" src={`/static/heroes/${heroes[3]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="weapon" src={`/static/weapons/${weapons[3]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="access" src={`/static/access/${access[3]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/cards/${cards[3]}.png`} />
            </div>
            <div className={`inner-box m-1`}>
              <img alt="card" src={`/static/relics/${relic}.png`} />
            </div>
          </div>
        </div>
        }

        {tab == 2 &&
        <div className="box w-100 my-1 p-1 tab">
          <b className="subtitle"><u>Timing des compétences en chaîne</u></b>
          <div>
          <div className="grid my-0">
            {/* If two patterns */}
            <div className="inner-box p-2">
            {pattern != "" && pattern != "1" &&
            <h3><b>Pattern 1</b></h3>
            }

            {pattern == "garam" &&
            <h4 className="pattern"><b>+60% HP</b></h4>
            }

            {pattern == "worm" &&
            <h4 className="pattern"><b>Charge</b></h4>
            }

            {pattern == "harvester" &&
            <h4 className="pattern"><b>+50% HP</b></h4>
            }

            {/* If one pattern */}
            {chain1.length == 0 &&
              <p><span className="no-infos">Non disponible</span></p>
            }
            {chain1.length > 0 &&
              <p><span>1-</span>{chain1}</p>
            }
            {chain2.length > 0 &&
              <p><span>2-</span>{chain2}</p>
            }
            {chain3.length > 0 &&
              <p><span>3-</span>{chain3}</p>
            }
            {chain4.length > 0 &&
              <p><span>4-</span>{chain4}</p>
            }
            {chain5.length > 0 &&
              <p><span>5-</span>{chain5}</p>
            }
            </div>

            {pattern != "1" &&
            <div className="box">
              
            {pattern != "" && pattern != "1" &&
            <h3><b>Pattern 2</b></h3>
            }

            {pattern == "garam" &&
            <h4 className="pattern"><b>-60% HP</b></h4>
            }

            {pattern == "worm" &&
            <h4 className="pattern"><b>Toupie</b></h4>
            }

            {pattern == "harvester" &&
            <h4 className="pattern"><b>-50% HP</b></h4>
            }

            {chainSpaceship1.length == 0 &&
              <p><span className="no-infos">Non disponible</span></p>
            }
            {chainSpaceship1.length > 0 &&
              <p><span>1-</span>{chainSpaceship1}</p>
            }
            {chainSpaceship2.length > 0 &&
              <p><span>2-</span>{chainSpaceship2}</p>
            }
            {chainSpaceship3.length > 0 &&
              <p><span>3-</span>{chainSpaceship3}</p>
            }
            {chainSpaceship4.length > 0 &&
              <p><span>4-</span>{chainSpaceship4}</p>
            }
            {chainSpaceship5.length > 0 &&
              <p><span>5-</span>{chainSpaceship5}</p>

            }
            </div>      
            }   
          </div>


          </div>
        </div>
        }

        {tab == 3 &&
        <div className="box w-100 my-1 p-1 tab">
          <b className="subtitle"><u>Informations complétementaires</u></b>
          <div className={`warning`}>
            <div className="content">
              <ul dangerouslySetInnerHTML={{ __html: warning }}></ul>
            </div>
          </div>
        <div>

          </div>
        </div>
        }
          </div>
        </div>


      </div>
    </div>
    </>
  );
}