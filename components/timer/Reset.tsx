import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';
import moment from 'moment';

export default function Reset() {
    const [remainingTime, setRemainingTime] = useState(0);
    useEffect(() => {
        const calculateRemainingTime = () => {
          const now = moment();
          const midnight = moment().set({ hour: 1, minute: 0, second: 0, millisecond: 0 });
          const nextMidnight = midnight.add(1, 'day');
    
          // Si c'est déjà après minuit, utilisez l'heure de minuit du jour suivant
          if (now.isAfter(midnight)) {
            nextMidnight.add(1, 'day');
          }
    
          const diff = nextMidnight.diff(now);
          setRemainingTime(diff);
        };
    
        // Calculer le temps restant au départ, puis mettre à jour toutes les secondes
        calculateRemainingTime();
        const timer = setInterval(calculateRemainingTime, 1000);
    
        // Nettoyez l'intervalle lorsque le composant est démonté
        return () => clearInterval(timer);
      }, []);
    
      const formatDuration = (duration) => {
        const hours = Math.floor(duration / 3600000);
        const minutes = Math.floor((duration % 3600000) / 60000);
        const formattedHours = hours.toString().padStart(2, '0');
        const formattedMinutes = minutes.toString().padStart(2, '0');
      
        return `${formattedHours}H${formattedMinutes}`;
      };

    return (
      <>
        <div className="item m-0 timer">
          <div className="content">
            <img className="icon" src="../static/icons/reset.svg"/>
            <small className="label"><b>Reset quotidien dans</b></small>
            <p className="timer m-0">{formatDuration(remainingTime)}</p>
          </div>
        </div>
      </>
      );
}