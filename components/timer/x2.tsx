import React, { useState, useEffect } from 'react';
import Moment from 'react-moment';

const X2: React.FC = () => {
  // State to store the current value of the state
  const [state, setState] = useState<number>(1);

  useEffect(() => {
    // Function to update the state every two weeks
    const updateState = () => {
      // Get the current date
      const currentDate = new Date();

      // Check if we are in the first week or the second week
      const isSecondWeek = Math.floor(currentDate.getDate() / 7) % 2 === 1;

      // Update the state accordingly
      if (isSecondWeek) {
        // If you're currently at step 3, set the state to 3; otherwise, set it to 1
        setState(currentDate.getDay() === 3 ? 3 : 1);
      }
    };

    // Update the state every two weeks
    const intervalId = setInterval(updateState, 14 * 24 * 60 * 60 * 1000);

    // Clean up the interval when the component is unmounted
    return () => clearInterval(intervalId);
  }, []); // Trigger the effect only on component mount

  // Component content based on the state value
  let content: React.ReactNode;
  switch (state) {
    case 1:
      content = <div>Content for state 1</div>;
      break;
    case 2:
      content = <div>Content for state 2</div>;
      break;
    case 3:
      content = <div>Content for state 3</div>;
      break;
    case 4:
      content = <div>Content for state 4</div>;
      break;
    default:
      content = <div>Default Content</div>;
  }

  return (
    <div>
      <h1>My Component</h1>
      {content}
    </div>
  );
};

export default X2;
