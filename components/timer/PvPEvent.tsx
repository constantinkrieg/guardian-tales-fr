import React, { useState, useEffect } from 'react';
import moment from 'moment';

export default function PvPEvent() {
  const [remainingTime, setRemainingTime] = useState(0);
  const [inSession, setInSession] = useState(false);
  const isWeekNumberEven = moment().isoWeek() % 2 === 0;

  useEffect(() => {
    const calculateRemainingTime = () => {
      const now = moment();

      let nextSessionStart;
      let nextSessionEnd;

      if (isWeekNumberEven) {
        nextSessionStart = moment().set({ hour: 13, minute: 0, second: 0, millisecond: 0 });
        nextSessionEnd = moment().set({ hour: 14, minute: 0, second: 0, millisecond: 0 });

        if (now.isBetween(nextSessionStart, nextSessionEnd)) {
          setRemainingTime(nextSessionEnd.diff(now));
          setInSession(true);
          return;
        }

        // If the current session is over, set the next session to session 2
        nextSessionStart = moment().set({ hour: 20, minute: 0, second: 0, millisecond: 0 });
        nextSessionEnd = moment().set({ hour: 21, minute: 0, second: 0, millisecond: 0 });
      } else {
        nextSessionStart = moment().set({ hour: 21, minute: 0, second: 0, millisecond: 0 });
        nextSessionEnd = moment().set({ hour: 4, minute: 0, second: 0, millisecond: 0 });
      }

      if (now.isBetween(nextSessionStart, nextSessionEnd)) {
        setRemainingTime(nextSessionEnd.diff(now));
        setInSession(true);
      } else {
        setInSession(false);

        // Determine time until the next session start
        if (now.isBefore(nextSessionStart)) {
          setRemainingTime(nextSessionStart.diff(now));
        } else {
          // If it's past the next session start, set the next session to the next day
          setRemainingTime(nextSessionStart.add(1, 'day').diff(now));
        }
      }
    };

    const timer = setInterval(calculateRemainingTime, 1000);

    // Clear the interval when the component unmounts
    return () => clearInterval(timer);
  }, [isWeekNumberEven]);

  const formatDuration = (duration) => {
    const hours = Math.floor(duration / 3600000);
    const minutes = Math.floor((duration % 3600000) / 60000);
    const formattedHours = hours.toString().padStart(2, '0');
    const formattedMinutes = minutes.toString().padStart(2, '0');

    return `${formattedHours}H${formattedMinutes}`;
  };

  return (
    <>
      {isWeekNumberEven && (
        <div className={`item timer ${inSession ? 'ongoing' : 'ended'}`}>
          <div className="content">
            <img className="icon" src="../static/icons/multi.svg" />
            <small className="label">
              <b>Arène {inSession ? <span>se termine</span> : <span>disponible</span>} dans</b>
            </small>
            <p className="timer m-0">{formatDuration(remainingTime)}</p>
          </div>
        </div>
      )}
      {!isWeekNumberEven && (
        <div className={`item timer ${inSession ? 'ongoing' : 'ended'}`}>
          <div className="content">
            <img className="icon" src="../static/icons/multi.svg" />
            <small className="label">
              <b>Co-op {inSession ? <span>se termine</span> : <span>disponible</span>} dans</b>
            </small>
            <p className="timer m-0">{formatDuration(remainingTime)}</p>
          </div>
        </div>
      )}
    </>
  );
}
