import React from "react";

export default function Boss({isActive, atr, element, onClick, name}) {

  return (
    <a href="#teams" className={`box ${isActive ? "active" : ""}`} onClick={onClick}>
      <img alt={atr} src={`/static/bosses/${atr}.png`} />
      <div>
        <p className="mb-0"><b>{name}</b> <img alt={element} className="elementBoss" src={`/static/elements/${element}.svg`} /></p>
      </div>
    </a>
  );
}