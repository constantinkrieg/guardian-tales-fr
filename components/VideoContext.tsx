import React, { createContext, useState, useContext } from 'react';

interface VideoContextType {
  // eslint-disable-next-line no-unused-vars
  showVideo: (url: string) => void;
  hideVideo: () => void;
  videoUrl: string;
  displayVideo: boolean;
}

const VideoContext = createContext<VideoContextType | null>(null);

export const useVideo = () => {
  const context = useContext(VideoContext);
  if (!context) {
    throw new Error('useVideo must be used within a VideoProvider');
  }
  return context;
};

// eslint-disable-next-line react/prop-types
export const VideoProvider = ({ children }) => {
  const [videoUrl, setVideoUrl] = useState('');
  const [displayVideo, setDisplayVideo] = useState(false);

  const showVideo = (url) => {
    setVideoUrl(url);
    setDisplayVideo(true);
  };

  const hideVideo = () => {
    setDisplayVideo(false);
    setVideoUrl('');
  };

  return (
    <VideoContext.Provider value={{ showVideo, hideVideo, videoUrl, displayVideo }}>
      {children}
    </VideoContext.Provider>
  );
};
