import React from "react";
import Team from "../../Team";

export default function Earth() {
  return (
    <>
    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","nar","cam","pli"]}
      weapons={["dol","nar","cam","pli"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-atk5","atk7-dark","crit-fire"]}    
      video="https://youtu.be/Mi5OcO2MEVE"
      videoSpaceship="https://www.youtube.com/watch?v=tEA63FlntSQ"
      relic="cup"
      pattern="worm"
      warning="
      <li><b>Difficulté & Survie extrême 💀</b></li>
      <li>La version avec Kamael est la plus envisageable étant donné qu'il permet de soigner légérement Andras</li>
      <li>Très facile de mourir au vu de la fragilité d'Andras contre ce boss. Si vous avez du mal, prenez une équipe en dessous</li>
      "
      chain1="Nari 1.0 > Andras 1.4 > Plitvice 0.7 > CA"
      chain2="Nari 0.8 > Andras 1.4 > Plitvice 0.7 > CA"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1="Nari 0.7 > Andras 1.4 > Plitvice 0.7 > CA"
      chainSpaceship2="Nari 0.7 > Andras 1.4 > Plitvice 0.7 > CA"
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />   

    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","chu","cam","pli"]}
      weapons={["dol","chu","cam","pli"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-light","atk7-dark","crit-fire"]}    
      video="https://www.youtube.com/watch?v=xAC2UnODomc"
      videoSpaceship="https://www.youtube.com/watch?v=iaWPEjObokA"
      relic="cup"
      pattern="worm"
      warning="
      <li><b>Difficulté & Survie extrême 💀</b></li>
      <li>La version avec Kamael est la plus envisageable étant donné qu'il permet de soigner légérement Andras</li>
      <li>Très facile de mourir au vu de la fragilité d'Andras contre ce boss. Si vous avez du mal, prenez une équipe en dessous</li>
      "      
      chain1="Andras 0.9 > Chun 1.2 > Lucy 1.0 + Vishuvac 0.7 + CA"
      chain2="Andras 0.9 > Chun 0.8 > Lucy 1.2 + Vishuvac 1.0 + CA"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1="Andras 0.6 > Chun 1.2 > Lucy 0.8 + Vishuvac 0.5 + CA"
      chainSpaceship2="Andras 0.5 > Chun 0.7 > Lucy 0.9 + Vishuvac 0.2 + CA"
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />  

    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","kam","cam","pli"]}
      weapons={["dol","kam","cam","pli"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-earth","atk7-dark","crit-fire"]}    
      video="https://youtu.be/3nkDCBhVjJQ"
      videoSpaceship="https://www.youtube.com/watch?v=t7edJKuVMSA"
      relic="cup"
      pattern="worm"
      warning="
      <li><b>Difficulté & Survie extrême 💀</b></li>
      <li>La version avec Kamael est la plus envisageable étant donné qu'il permet de soigner légérement Andras</li>
      <li>Très facile de mourir au vu de la fragilité d'Andras contre ce boss. Si vous avez du mal, prenez une équipe en dessous</li>
      "        
      chain1="Andras 1.1 > Plitvice 1.4 > Kamael 1.1 > CA > Cammie 1.9"
      chain2="Andras 1.1 > Plitvice 1.4 > Kamael 1.1 > CA > Cammie 1.5"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1="Andras 1.0 > Plitvice 1.1 > Kamael 1.1 > CA > Cammie 2.1"
      chainSpaceship2="Andras 0.5 > Plitvice 1.1 > Kamael 1.1 > CA > Cammie 2.1"
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />  

    <Team onVideoPlay={onVideoPlay} 
      heroes={["rey","sci","win","lil"]}
      weapons={["rey","sci","win","lil"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="42.9" cards={["skill-skill","crit-crit","crit-fire","atk7-dark"]}    
      video="https://www.youtube.com/watch?v=NUQcVzj_dpM"
      videoSpaceship="https://www.youtube.com/watch?v=Qz00fkkx8sE"
      relic="cup"
      pattern="worm"
      warning="
      <li>Placez les grandes flaques d'acide loin de votre équipe afin qu'elle ne soit pas gênées par elles.</li>
      <li>Si vous obtenez le pattern Charge, assurez-vous de laisser le boss foncez dans l'un des rochers, pour le pattern Toupie vous pouvez juste l'étourdir tout de suite. C'est la seule différence entre les deux combats.</li>
      "      
      chain1="Lilith 0.1 > Scintilla 1.0-1.1 + CA > Rey 0.1 > Winling 1.0-1.1 + CA"
      chain2="Lilith 0.1 > Scintilla 1.0-1.1 + CA > Rey 0.1 > Winling 1.0-1.1 + CA"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1="Lilith 0.1 > Scintilla 1.0-1.1 > CA > Rey 0.1 > Winling 1.0-1.1 + CA"
      chainSpaceship2="Lilith 0.1 > Scintilla 1.0-1.1 > CA > Rey 0.1 > Winling 1.0-1.1 + CA"
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["elv","luc","euh","vis"]}
      weapons={["elv","luc","fire","vis"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-fire","atk5-atk5","crit-atk7"]}    
      video="https://youtu.be/WnqhSAXFia4?si=c4OmJmv6yJ040_hr"
      videoSpaceship=""
      relic="cup"
      pattern="worm"
      warning="
      <li><b>Si le niveau du boss est inférieur à 86, les teams Elvira sont les plus puissantes dû à son passif. 🔻<br/>Après ce seuil plus le niveau du boss est élevé, moins Elvira fera de dégâts, et sera donc dépasser par d'autres équipes.</b></li>
      <li>Pour les cartes et les reliques, vous pouvez utiliser des cartes Compétences ou Crit sur Elvira (en changeant bien sûr pour les autres également). Dégâts de compétence est meilleur pour un potentiel de dégâts élevés tandis que Crit est pour des dégâts plus réguliers.</li>
      <li>Le timing des chaînes n'est pas fixe. L'important est que Lucy et Eunha soient <1.0 et que vous utilisiez la chaîne d'Elvira tant que vous avez encore la chaîne de Lucy et les buffs des compétences de Vishuvac. Le timing exact dépend de la façon dont les compétences de Vishuvac s'alignent.</li>
      <li>Environ 45 secondes après le début du combat, allez vers Vishuvac pour que la fosse d'acide la distraie, ce qui retardera sa compétence, sinon elle ne s'alignera pas avec la 2ème chaîne.</li>
      <li><b>Il est important de s'entraîner à avoir la bonne distance pour obtenir 61% avec la compétence d'arme. Si vous obtenez le pattern Charge, le premier coup de la compétence d'arme (qui fait 40% si vous vous positionnez correctement) doit stun le ver, car le second coup ne le touchera plus autrement</b></li>
      "      
      chain1="Lucy 0.1 > Eunha 0.7 + Vishuvac 0.2 > CA > Elvira 2.0"
      chain2="Lucy 0.1 > Eunha 0.7 + Vishuvac 0.2 > CA > Elvira 2.0"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["elv","chu","luc","vis"]}
      weapons={["elv","chu","luc","vis"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-light","atk5-fire","crit-atk7"]}    
      video="https://www.youtube.com/watch?v=USlzmF0fMF8"
      videoSpaceship=""
      relic="cup"
      pattern="worm"
      warning="
      <li><b>Si le niveau du boss est inférieur à 86, les teams Elvira sont les plus puissantes dû à son passif. 🔻<br/>Après ce seuil plus le niveau du boss est élevé, moins Elvira fera de dégâts, et sera donc dépasser par d'autres équipes.</b></li>
      <li>Pour les cartes et les reliques, vous pouvez utiliser des cartes Compétences ou Crit sur Elvira (en changeant bien sûr pour les autres également). Dégâts de compétence est meilleur pour un potentiel de dégâts élevés tandis que Crit est pour des dégâts plus réguliers.</li>
      <li>Le timing des chaînes n'est pas fixe. L'important est que Lucy et Eunha soient <1.0 et que vous utilisiez la chaîne d'Elvira tant que vous avez encore la chaîne de Lucy et les buffs des compétences de Vishuvac. Le timing exact dépend de la façon dont les compétences de Vishuvac s'alignent.</li>
      <li>Environ 45 secondes après le début du combat, allez vers Vishuvac pour que la fosse d'acide la distraie, ce qui retardera sa compétence, sinon elle ne s'alignera pas avec la 2ème chaîne.</li>
      <li><b>Il est important de s'entraîner à avoir la bonne distance pour obtenir 61% avec la compétence d'arme. Si vous obtenez le pattern Charge, le premier coup de la compétence d'arme (qui fait 40% si vous vous positionnez correctement) doit stun le ver, car le second coup ne le touchera plus autrement</b></li>
      "      
      chain1="Lucy 0.1 > Chun °0.6 + Vishu 2.4 > CA > Elvira 2.4"
      chain2="Lucy 0.1 > Chun °0.6 + Vishu > CA > Elvira 2.4"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=" "
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["euh","nar","kan","lah"]}
      weapons={["euh","nar","kan","lah"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="36.1" cards={["skill-skill","atk7-crit","atk5-atk5","crit-crit"]}    
      video="https://www.youtube.com/watch?v=zI4n1DsOS1I"
      videoSpaceship="https://www.youtube.com/watch?v=RJi9JlULcPA"
      relic="cup"
      pattern="worm"
      warning="
      <li><b>Le timing de la compétence d'arme à la fin de la 2ème chaîne est important. Environ >= 1.0 pour le pattern Charge, <= 0.8 pour le pattern Toupie.</b>
      <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss. </b></li>
      <li>Pour le 2ème stun, utilisez la compétence d'arme 29 secondes après le début du combat pour le Charge Pattern et vers 31,5 secondes après le début du combat pour le Spin Pattern. Vous voulez utiliser la chaîne de Kanna <0.8, sans perdre la chaîne d'Eunha à la fin.
      <li>Pour le 3ème stun, utilisez la compétence d'arme 50 secondes après le début du combat."   
      chain1="Kanna 2.4 > Nari 2.2 > Lahn 1.8 > Eunha 1.9"
      chain2="Kanna 1.0 > Nari 2.2 > Lahn 1.8 > Eunha 1.9 > CA 1.0"
      chain3="Kanna 0.3 > Nari 2.2 > Lahn 1.8 > Eunha 1.9"
      chain4=""
      chain5=""
      chainSpaceship1="Kanna 2.4 > Nari 2.2 > Lahn 1.8 > Eunha 1.9"
      chainSpaceship2="Kanna 0.5 > Nari 2.2 > Lahn 1.8 > Eunha 1.9 > CA 0.8"
      chainSpaceship3="Kanna 0.3 > Nari 2.2 > Lahn 1.8 > Eunha 1.9"
      chainSpaceship4=""
      chainSpaceship5=""
    /> 

    <Team onVideoPlay={onVideoPlay} 
      heroes={["elv","luc","nar","vis"]}
      weapons={["elv","luc","nar","vis"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-fire","atk5-atk5","crit-atk7"]}    
      video="https://youtu.be/WnqhSAXFia4?si=c4OmJmv6yJ040_hr"
      videoSpaceship=""
      relic="cup"
      pattern="worm"
      warning="
      <li>Pour les cartes et les reliques, vous pouvez utiliser des cartes Compétences ou Crit sur Elvira (en changeant bien sûr pour les autres également). Dégâts de compétence est meilleur pour un potentiel de dégâts élevés tandis que Crit est pour des dégâts plus réguliers.</li>
      <li>Le timing des chaînes n'est pas fixe. L'important est que Lucy et Eunha soient <1.0 et que vous utilisiez la chaîne d'Elvira tant que vous avez encore la chaîne de Lucy et les buffs des compétences de Vishuvac. Le timing exact dépend de la façon dont les compétences de Vishuvac s'alignent.</li>
      <li>Les timings de la chaîne ne sont pas fixes. L'important est que Lucy et Nari soient <1.0. Les temps exacts dépendent de la façon dont les compétences de Vishuvac s'alignent.</li>
      <li>Environ 45 secondes après le début du combat, allez vers Vishuvac pour que la fosse d'acide la distraie, ce qui retardera sa compétence, sinon elle ne s'alignera pas avec la 2ème chaîne.</li>
      "      
      chain1="Lucy 0.1 > Nari 0.7 > Vishuvac 2.1 > CA > Elvira 2.2"
      chain2="Lucy 0.1 > Nari 0.5 > Vishuvac 2.4 > CA > Elvira 2.1"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["rey","sci","lil","yuz"]}
      weapons={["rey","sci","lil","fire"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["skill-skill","crit-crit","atk7-dark","crit-fire"]}    
      video="https://youtu.be/TR3Ta7Qh43c"
      videoSpaceship=""
      relic="book"
      pattern="worm"
      warning=""      
      chain1="Rey 0.5 > CA > Scintilla 1.0 > Lilith 0.5 > CA > Yuze 1.0"
      chain2="Rey 0.5 > CA > Scintilla 1.0 > Lilith 0.5 > CA > Yuze 1.0"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />                     
    </>
  );
}
