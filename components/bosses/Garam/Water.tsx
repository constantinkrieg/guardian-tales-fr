import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>

        <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","and","cam","kam"]}
        weapons={["ame","earth","cam","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="53.4" cards={["skill-skill","crit-crit","atk7-dark","crit-earth"]}    
        video="https://www.youtube.com/watch?v=KCMgRleuibI"
        videoSpaceship="https://www.youtube.com/watch?v=jM6YowPcpHg"
        relic="cup"
        pattern="garam"
        warning="
        <li>Faites attention à Cammie pendant sa première chaîne, afin de ne pas annuler sa marque avec sa chaîne.</li>
        <li>Essayez de garder Garam près du centre, afin qu'il ne soit pas hors de portée des alliés, ce qui est particulièrement important pour Andras en raison de sa courte portée.</li>
        <li>Vous pouvez le faire se téléporter sur vous, ce qui le maintient au centre.</li>
        <li>Pour <50%, retardez légèrement la première Compétence d'arme pour être sûr qu'il touche Garam.</li>
        "
        chain1="Kamael 0.1 > Cammie 0.6 > Compétence d'arme > Ameris 1.4"           
        chain2="-"
        chain3="Kamael 0.5 > Cammie 1.1 > Compétence d'arme > Ameris 1.4"
        chain4=""
        chain5=""
        chainSpaceship1="Kamael 0.1 > Cammie 0.6 > Compétence d'arme > Ameris 1.4 > Andras 0.3"
        chainSpaceship2="-"
        chainSpaceship3="Kamael 0.5 > Cammie 1.2 > Compétence d'arme > Ameris 1.5"
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","kam","tin","and"]}
        weapons={["ame","kam","tin","earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45.7" cards={["skill-skill","atk7-atk5","crit-earth","crit-crit"]}    
        video="https://www.youtube.com/watch?v=cYMA2lxhGkQ"
        videoSpaceship=""
        relic="cup"
        pattern="garam"
        warning="
        <li><b>Pas sûr que les mêmes chaînes fonctionnent pour le pattern 2 | -50%. Si ce n'est pas le cas, si vous utilisez la chaîne d'Andras à 1.4 ou plus tôt pendant la 2ème chaîne, ça devrait fonctionner.</b></li>
        <li>Pour la relique d'Ameris, utilisez de préférence une avec de bonnes stats en Crit.</li>
        <li>Essayez de garder le boss près du centre, afin qu'il ne soit pas hors de portée de vos alliés, ce qui est particulièrement important pour Andras en raison de sa courte portée.</li>
        <li>Vous pouvez le faire se téléporter sur vous, ce qui le maintient au centre.</li>
        "
        chain1="Kamael 0.8 > Tinia 0.8 > Compétence d'Arme > Ameris 1.4"           
        chain2="-"
        chain3="Kamael 0.2 > Tinia 0.8 > Compétence d'Arme > Ameris 1.4"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
      
      <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","euh","kam","may"]}
        weapons={["ame","earth","kam","may"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="44.3" cards={["skill-skill","crit-atk7","atk5-earth","crit-crit"]}    
        video="https://www.youtube.com/watch?v=C4qndmcBJdM"
        videoSpaceship="https://www.youtube.com/watch?v=tPH3s1I1IZ0"
        relic="cup"
        pattern="garam"
        warning="
        <li>Vous devez garder le boss au centre, sinon Kamael ne le touchera pas pendant la première chaîne.</li>
        <li>Vous pouvez le faire se téléporter sur vous, ce qui le maintient au centre et facilite l'esquive de son attaque en cône.</li>
        <li>Pour la relique d'Ameris, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        "
        chain1="Ameris 1.5 > Eunha 0.1 > Compétence d'Arme > Kamael 0.1"           
        chain2="Ameris 1.5 > Eunha 0.1 > Compétence d'Arme > Kamael 0.1"
        chain3="Ameris 1.7"
        chain4=""
        chain5=""
        chainSpaceship1="Ameris 1.5 > Eunha 0.1 > Compétence d'Arme > Kamael 0.1"
        chainSpaceship2="Ameris 1.5 > Mayreel 0.1"
        chainSpaceship3="Kamael 1.1"
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","tin","kam","may"]}
        weapons={["ame","tin","kam","may"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="43.6" cards={["skill-skill","crit-atk7","atk5-earth","crit-crit"]}    
        video="https://youtu.be/qo5h8QGJVXQ"
        videoSpaceship="https://youtu.be/yT26UxkIvHg"
        relic="cup"
        pattern="garam"
        warning="
        <li><b>Pas sûr que les mêmes chaînes fonctionnent pour le pattern 2 | -50%. Si ce n'est pas le cas, si vous utilisez la chaîne de Mayreel à 0.8 ou plus tôt pendant la 2ème chaîne, ça devrait fonctionner.</b></li>
        <li>Essayez de garder le boss près du centre, afin qu'il ne soit pas hors de portée de vos alliés.</li>
        <li>Vous pouvez le faire se téléporter sur vous, ce qui le maintient au centre et facilite l'esquive de l'attaque swipe (seulement <50% hp pattern).</li>
        "
        chain1="Ameris 1.5 > Mayreel 0.7 > Tinia 0.8 > Compétence d'Arme > Kamael 1.1"           
        chain2="Ameris 1.5 > Mayreel 0.7 > Compétence d'Arme"
        chain3="Kamael 1.3 > Tinia 0.9"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["rue","yuz","bar","lil"]}
        weapons={["ame","earth","bar","lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="40.2" cards={["skill-skill","crit-earth","atk7-crit","crit-dark"]}    
        video="https://www.youtube.com/watch?v=XtilY9ax09Y"
        videoSpaceship=""
        relic="cup"
        pattern="garam"
        warning=""
        chain1=""           
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["rue","yuz","rey","lil"]}
        weapons={["rue","earth","earth","lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="35.3" cards={["skill-skill","crit-atk7","atk5-earth","crit-dark"]}    
        video="https://www.youtube.com/watch?v=VjJmb4tApPo"
        videoSpaceship=""
        relic="cup"
        pattern="garam"
        warning="
        <li>🙂</li>
        <li><b>Mettre collier Miroir sur Rue - Seulement si il a de bonnes statistiques en Vitesse de TdR. Elle n'a pas besoin du Crit procuré par le collier Minotaure.</b></li>
        "
        chain1="Rey 1.5 > Rue 1.2 > Compétence d'Arme > Yuze 1.2"           
        chain2="Lilith 0.5 > CA (Sur Abattu ASAP)"
        chain3="Rey 1.5 > Rue 1.2 > Compétence d'Arme > Yuze 1.2"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}