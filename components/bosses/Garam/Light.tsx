import React from "react";
import Team from "../../Team";

export default function Light() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "lah", "kan", "nar"]}
        weapons={["euh", "lah", "kan", "nar"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-crit", "atk5-atk5", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=xJy1H-FA0gw"
        videoSpaceship=""
        relic="cup"
        pattern="garam"
        warning="
        <li><b> Pas de vidéo Pattern >60% HP disponible, on suppose que c'est les mêmes chaînes.</b></li>
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss. </b></li>
        <li>Placez-vous loin de votre équipe lorsque vous utilisez la chaîne de Kanna, afin que Nari et Lahn ne soient pas perturbés par la direction que prendrait le saut de Garam.</li>
        <li>Mettez de préférence des Skill DMG en stats sur la relique d'Euhna.</li>
        "
        chain1="Nari (0.1) > Kanna (0.4) > Euhna (1.7) > CA > Lahn (1.5)"
        chain2="Nari (0.5) > Kanna (0.4) > Euhna (1.7) > CA > Lahn (1.4)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

    <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "kni"]}
        weapons={["euh", "nar", "kan", "kni"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video="https://www.youtube.com/watch?v=HedVFGAhz_M"
        videoSpaceship=""
        relic="cup"
        pattern="garam"
        warning="
        <li><b> Pas de vidéo Pattern >60% HP disponible, on suppose que c'est les mêmes chaînes.</b></li>
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss. </b></li>
        <li>Mettez de préférence des Skill DMG en stats sur la relique d'Euhna.</li>
        "
        chain1="Nari (0.1) > Kanna (0.1) > Euhna (1.5) > CA > Chevalier (0.7)"
        chain2="Nari (0.2) > Kanna (1) + Chevalier (0.3) > CA > Euhna (1.7) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "fk"]}
        weapons={["euh", "nar", "kan", "fk"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video="https://youtu.be/u2ERR6RJzSo"
        videoSpaceship=""
        relic="cup"
        pattern="garam"
        warning="
        <li><b>Pas de vidéo Pattern >60% HP disponible, on suppose que c'est les mêmes chaînes.</b></li>
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss. </b></li>
        <li>Mettez de préférence des Skill DMG en stats sur la relique d'Euhna.</li>
        "
        chain1="Nari (0.3) > Kanna (0.2) > Chevalier du Futur (1.3) > CA"
        chain2="Euhna (2.4) > CA"
        chain3="Nari (0.3) > Kanna (0.2) > Chevalier du Futur (1.6) > CA"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "ros", "kan"]}
        weapons={["ido", "nar", "fk", "kan"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "crit-atk7", "atk5-atk5", "atk5-atk5"]}
        video="https://www.youtube.com/watch?v=z5mQQWYkOn8"
        videoSpaceship="https://youtu.be/Fw7yD-sjey4 "
        relic="cup"
        pattern="garam"
        warning=""
        chain1="Kanna (0.5) > Nari (1.1)"
        chain2="// Stun uniquement"
        chain3="Kanna (0.5) > Nari (1.1)"
        chain4=""
        chain5=""
        chainSpaceship1="Kanna (0.1) > Nari (0.4)"
        chainSpaceship2="Kanna (0.2) > Naris (0.3)"
        chainSpaceship3="// Stun uniquement"
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "kan", "kni"]}
        weapons={["ido", "nar", "kan", "kni"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "crit-atk7", "atk5-atk5", "atk5-atk5"]}
        video="https://youtu.be/H-1KfpLkuvs"
        videoSpaceship="https://youtu.be/uz-iPo10_Oc"
        relic="cup"
        pattern="garam"
        warning=""
        chain1="Kanna (0.3) > Nari (1.9) > CA > Chevalier (0.1) > Andras (0.8)"
        chain2="// Stun uniquement"
        chain3="Kanna (0.3) > Nari (1.9) > CA > Chevalier (0.1) > Andras (0.8)"
        chain4=""
        chain5=""
        chainSpaceship1="Kanna (0.3) > Nari (1.5) > CA > Chevalier (0.1) > Andras (1.2)"
        chainSpaceship2="// Stun uniquement"
        chainSpaceship3="Kanna (0.3) > Nari (1.5) > CA > Chevalier (0.1) > Andras (1.2)"
        chainSpaceship4=""
        chainSpaceship5=""
      />                   
    </>
  );
}
