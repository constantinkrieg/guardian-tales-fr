import React from "react";
import Team from "../../Team";
 
export default function Light() {
  return (
    <>
        <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","vin","cam","ogh"]}
        weapons={["cla","vin","cam","ogh"]}
        dmg="48.1" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}
        access={["mino","sg","sg","sg"]}
        video="https://www.youtube.com/watch?v=1ukzngK18so"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Utiliser la CA pour le 2e stun à 27.5 secondes de la fin du combat (soit -32.5 secondes après le début du combat).</li>
        <li>Utiliser la CA pour le 3e stun à 7 secondes de la fin du combat (soit -53 secondes après le début du combat). </li>
        <li>Apparemment, la variance des dégâts de cette équipe est élevée.
        "
        chain1="Claude 1.7 > Oghma 0.9 > Cammie 2.4 > CA > Vinette 1.1"
        chain2="Claude 1.5 > Oghma 0.9 > Cammie 2.4 > CA > Vinette 1.1"
        chain3="Claude 0.3 > CA"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />
    </>
  );
}