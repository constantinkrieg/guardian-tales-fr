import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>

    <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","cam","gab"]}
        weapons={["and","chu","cam","gab"]}
        access={["mino", "sg", "minotaur", "minotaur"]}
        dmg="43.4" cards={["skill-skill","atk5-atk5","atk5-dark","atk7-light"]}
        video="https://www.youtube.com/watch?v=Lr56BPdgZmk"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Puisque l'équipe a 2 bonus de groupe qui augmentent les taux de coups critiques vous devriez éviter d'en ajouter plus, donc vous êtes mieux d'utiliser les cartes et une relique avec du Dêgats de compétence sur Andras. Ainsi que de prioriser des reliques avec des stats plus en ATK qu'en Crit pour vos alliés. Pour la même raison, il est préférable d'utiliser des collier Mino Lumière et Sombre si vous en avez sur Gabrielle et Cammie plutôt que des lunettes.</li>
        <li>Amenez le boss contre le mur pour que les alliés soient suffisamment éloignés pour empêcher les dashs de se produire et aussi en ayant le boss contre le mur et en étant loin de lui, ses slashs ne vous frapperont pas, ce qui aidera à la survie.</li>
        <li>Marcher en diagonale pendant les météores pour subir moins de dégâts</li>
        <li>Utilisez la chaîne de Chun après le dernier coup de sa séquence d'attaque normale si possible, mais entre 1.1 et 1.7</li>"
        chain1="Andras 2.1 > Chun 1.7 > Compétence d'arme > Gabriel 1.2 > Cammie 2.4"
        chain2="Andras 1.5 > Chun 1.1 > Compétence d'arme > Gabriel 1.2 > Cammie 0.5"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  
      
      <Team onVideoPlay={onVideoPlay} 
        heroes={["sha","lap","val","kai"]}
        weapons={["sha","lap","val","kai"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="41.4" cards={["skill-crit","atk7-light","atk5-atk5","crit-crit"]}
        video="https://www.youtube.com/watch?v=weidilMwp00"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>Difficulté & Survie compliquée avec Shapira 💀 - Si le boss fait son dash, vous allez perdre beaucoup de temps et de dêgats. On recommande malgré les dégâts de jouer plutôt Distance sur ce boss.</b></li>
        "
        chain1="Shapira 2.2 > Valencia 1.4 > Lapice 1.2 > Compétence d'arme > Kai 1.3"
        chain2="Shapira 2.5 > Valencia 1.4 > Lapice 1.2 > Compétence d'arme > Kai 1.3"
        chain3="Shapira 2.5"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","cam","elv"]}
        weapons={["and","chu","cam","elv"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-light","atk5-dark","atk7-crit"]}
        video="https://www.youtube.com/watch?v=7ydF7usrTKY"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li><b>Si le niveau du boss est inférieur à 86, les teams Elvira sont les plus puissantes dû à son passif. 🔻<br/>Après ce seuil plus le niveau du boss est élevé, moins Elvira fera de dégâts, et sera donc dépasser par d'autres équipes.</b></li>
        <li>Marcher en diagonale pendant les météores pour subir moins de dégâts</li>
        <li>Utilisez la chaîne de Chun après le dernier coup de sa séquence d'attaque normale pour appliquer son débuff Def distance au boss</li>"
        chain1="Andras 1.4 > Chun 1.1 > Compétence d'arme > Elvira 1.5 > Cammie 2.2"
        chain2="Andras 1.4 > Chun 0.9 > Compétence d'arme > Elvira 1.5 > Cammie 0.4"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay} 
        heroes={["sha","val","lap","car"]}
        weapons={["sha","val","lap","car"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="39" cards={["skill-skill","atk5-atk7","atk5-light","atk5-atk5"]}
        video="https://www.youtube.com/watch?v=xSjYx-cnOBk"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li><b>Difficulté & Survie compliquée avec Shapira 💀 - Si le boss fait son dash, vous allez perdre beaucoup de temps et de dêgats. On recommande malgré les dégâts de jouer plutôt Distance sur ce boss.</b></li>
        "
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      /> 

<Team onVideoPlay={onVideoPlay} 
        heroes={["cam","vin","ogh","cla"]}
        weapons={["cam","vin","ogh","cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=PTCkUkcQDXo"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li><b>En ce qui concerne Oghma ou Cammie comme lead, selon les joueurs coréens, Oghma est meilleur, mais utiliser Cammie pour tuer le boss peut mener à un meilleur Carry-Over.</b></li>
        <li>L'autre chose qui rentre en jeu est bien sûr les stats sur leurs deux armes, pour le lead, on privilégie la vitesse de recharche de la compétence d'arme et les dégâts de compétence.</li>
        <li>La chaîne de Vinette est utilisée après son dernier coup de la chaîne d'attaque normale (appliquant la réduction de défense)</li>
        <li>Retarder la compétence d'arme avant le 2ème stun 33 secondes après sur le début de votre combat</li>
        <li>Retarder la compétence d'arme avant le 3ème stun 55.5 secondes après sur le début de votre combat</li>

        <li>Grâce au bonus de groupe de Vinette, il n'est pas nécessaire de bloquer le boss au début du combat, car vous pouvez de toute façon le stun à temps pour les météores.</li>
        "
        chain1="Cammie 2.2 > Vinette 1.4 > Claude 2.2 > CA > Oghma 0.2"
        chain2="Cammie 1.6 > Vinette 1.3 > Claude 2.1 > CA > Oghma 0.3"
        chain3="Cammie 1.3 > Vinette 1.6"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["ogh","vin","cam","cla"]}
        weapons={["ogh","vin","cam","cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=5m3uxgB6u5A"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li><b>En ce qui concerne Oghma ou Cammie comme lead, selon les joueurs coréens, Oghma est meilleur, mais utiliser Cammie pour tuer le boss peut mener à un meilleur Carry-Over.</b></li>
        <li>L'autre chose qui rentre en jeu est bien sûr les stats sur leurs deux armes, pour le lead, on privilégie la vitesse de recharge de la compétence d'arme et les dégâts de compétence.</li>
        <li>Grâce au bonus de groupe de Vinette, il n'est pas nécessaire de bloquer le boss au début du combat, car vous pouvez de toute façon le stun à temps pour les météores.</li>
        "
        chain1="Vinette 0.2 > Claude 2.4 > CA > Oghma 1.0 > Cammie 0.6 > CA"
        chain2="Vinette 0.8 > Claude 2.4 > CA > Oghma 0.5 > Cammie 0.5 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />


      <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","ogh","cam","vin"]}
        weapons={["cla","ogh","cam","vin"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="34" cards={["skill-skill","crit-crit","crit-dark","atk5-atk7"]}
        video="https://www.youtube.com/watch?v=Z7LnSprc4ZE"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />   
</>
  );
}