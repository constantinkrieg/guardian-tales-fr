import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>

        <Team onVideoPlay={onVideoPlay} 
        heroes={["cam","vin","ogh","cla"]}
        weapons={["cam","vin","ogh","cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=PTCkUkcQDXo"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li><b>En ce qui concerne Oghma ou Cammie comme lead, selon les joueurs coréens, Oghma est meilleur, mais utiliser Cammie pour tuer le boss peut mener à un meilleur Carry-Over.</b></li>
        <li>L'autre chose qui rentre en jeu est bien sûr les stats sur leurs deux armes, pour le lead, on privilégie la vitesse de recharche de la compétence d'arme et les dégâts de compétence.</li>
        <li>La chaîne de Vinette est utilisée après son dernier coup de la chaîne d'attaque normale (appliquant la réduction de défense)</li>
        <li>Retarder la compétence d'arme avant le 2ème stun 33 secondes après sur le début de votre combat</li>
        <li>Retarder la compétence d'arme avant le 3ème stun 55.5 secondes après sur le début de votre combat</li>

        <li>Grâce au bonus de groupe de Vinette, il n'est pas nécessaire de bloquer le boss au début du combat, car vous pouvez de toute façon le stun à temps pour les météores.</li>
        "
        chain1="Cammie 2.2 > Vinette 1.4 > Claude 2.2 > CA > Oghma 0.2"
        chain2="Cammie 1.6 > Vinette 1.3 > Claude 2.1 > CA > Oghma 0.3"
        chain3="Cammie 1.3 > Vinette 1.6"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["ogh","vin","cam","cla"]}
        weapons={["ogh","vin","cam","cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=5m3uxgB6u5A"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li><b>En ce qui concerne Oghma ou Cammie comme lead, selon les joueurs coréens, Oghma est meilleur, mais utiliser Cammie pour tuer le boss peut mener à un meilleur Carry-Over.</b></li>
        <li>L'autre chose qui rentre en jeu est bien sûr les stats sur leurs deux armes, pour le lead, on privilégie la vitesse de recharge de la compétence d'arme et les dégâts de compétence.</li>
        <li>Grâce au bonus de groupe de Vinette, il n'est pas nécessaire de bloquer le boss au début du combat, car vous pouvez de toute façon le stun à temps pour les météores.</li>
        "
        chain1="Vinette 0.2 > Claude 2.4 > CA > Oghma 1.0 > Cammie 0.6 > CA"
        chain2="Vinette 0.8 > Claude 2.4 > CA > Oghma 0.5 > Cammie 0.5 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","vin","cam","ogh"]}
        weapons={["cla","vin","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=3HpNies6ApU"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Grâce au bonus de groupe de Vinette, il n'est pas nécessaire de bloquer le boss au début du combat, car vous pouvez de toute façon le stun à temps pour les météores.</li>
        <li>Retarder la compétence d'arme avant le 2ème stun 33 secondes après sur le début de votre combat, et 55.5 secondes après le début du combat pour utiliser la compétence pour le 3ème stun.</li>
        "
        chain1="Claude 1.3 > Oghma 1.1 > Cammie 2.4 > CA"
        chain2="Claude 1.3 > Oghma 0.8 > Cammie 1.9 > CA > Vinette 0.4"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","kam","cam","ogh"]}
        weapons={["cla","dark","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=sqCQazDNA-E"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Vous devez kiter le boss dès le début. Le but est d'obtenir la deuxième série d'attaques (Triple Slash) très tôt. Le boss a l'aggro sur Oghma, donc vous devez faire en sorte qu'il soit assez proche d'Oghma pour utiliser l'attaque. Si le commandant fait l'attaque de vague à 13 secondes après le début du combat, vous l'avez fait correctement et il n'utilisera pas de météores. Si le triple slash est retardé trop longtemps, il utilisera les météores.</li>
        <li>Le boss essaiera de vous contourner lorsque vous le bloquerez, vous devrez donc vous déplacer un peu pour éviter qu'il ne vous contourne.</li>
        <li>Si vous voyez que votre Kamaël ne garantie pas à 100% la réduction de défense, vous pouvez utiliser son arme exclusive. Ou si celui-çi est MLB contrairement à votre arme Dark, privilégiez bien évidemment l'option qui fait le plus de dégâts.</li>
        "
        chain1="Claude 1.4 > Oghma 1.8 > Cammie 2.1 > CA > Kamael 1.1"
        chain2="Claude 1.5 > Oghma 1.3 > Cammie 2.4 > CA > Kamael 1.1"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","nar","cam","ogh"]}
        weapons={["cla","nar","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=zf80eF_u_-s"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Vous devez kiter le boss dès le début. Le but est d'obtenir la deuxième série d'attaques (Triple Slash) très tôt. Le boss a l'aggro sur Oghma, donc vous devez faire en sorte qu'il soit assez proche d'Oghma pour utiliser l'attaque. Si le commandant fait l'attaque de vague à 13 secondes après le début du combat, vous l'avez fait correctement et il n'utilisera pas de météores. Si le triple slash est retardé trop longtemps, il utilisera les météores.</li>
        <li>Le boss essaiera de vous contourner lorsque vous le bloquerez, vous devrez donc vous déplacer un peu pour éviter qu'il ne vous contourne.</li>
        "
        chain1="Claude 1.7 > Oghma 1.5 > Cammie 2.3 > CA "
        chain2="Claude 1.7 > Oghma 1.1 > Cammie 2.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","chu","cam","ogh"]}
        weapons={["cla","chu","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=SPLtpW_SxiQ"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Vous devez kiter le boss dès le début. Le but est d'obtenir la deuxième série d'attaques (Triple Slash) très tôt. Le boss a l'aggro sur Oghma, donc vous devez faire en sorte qu'il soit assez proche d'Oghma pour utiliser l'attaque. Si le commandant fait l'attaque de vague à 13 secondes après le début du combat, vous l'avez fait correctement et il n'utilisera pas de météores. Si le triple slash est retardé trop longtemps, il utilisera les météores.</li>
        <li>Le boss essaiera de vous contourner lorsque vous le bloquerez, vous devrez donc vous déplacer un peu pour éviter qu'il ne vous contourne.</li>
        "
        chain1="Claude 1.2 > Oghma 1.2 > Cammie 2.4 > CA "
        chain2="Claude 1.5 > Oghma 1.0 > Cammie 2.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","cam","kar","ogh"]}
        weapons={["cla","cam","kar","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://youtu.be/-tf3wwq9WGs"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Claude 1.6 > Oghma 1.0 > Cammie > 2.4 > CA > Karina 1.1"
        chain2="Claude 1.6 > Oghma 1.0 > Cammie > 2.4 > CA > Karina 1.1"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
