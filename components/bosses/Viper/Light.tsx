import React from "react";
import Team from "../../Team";
 
export default function Light({ onVideoPlay }) {
  return (
    <>

          <Team onVideoPlay={onVideoPlay}
            heroes={["euh","kan","nar","fk"]}
            weapons={["euh","kan","nar","fk"]}
            dmg="51" cards={["skill-skill","atk5-atk5","crit-atk7","crit-crit"]}
            access={["sg","sg","sg","sg"]}
            video="/static/videos/s62-basicfk-viper.mp4"
            videoSpaceship=""
            relic="cup"
            pattern="1"
            warning="
            <li>Il est plus safe de jouer avec Lahn en terme de performance constante et régulière qu'avec FK</li>
            <li>Afin de réduire la RNG avec l'aggro de FK, on vous conseille de le mettre en dernière position</li>
            <li>Ne pas utiliser la marque d'Euhna (Attaque normale secondaire).</li>
            <li>Bloquer le boss jusqu'à peu de temps avant la 2ème chaîne, car il ne se téléporte pas avant d'avoir donné le coup de poing. L'emplacement de la téléportation est aléatoire, donc les dégâts dépendront un peu de l'endroit où il se téléporte en plus de l'aléatoire du Crit.</li>
            <li>On ne sais pas si les dégâts moyens seront meilleur que ceux de Lahn, cette équipe est aussi plus difficile à jouer, donc essayez les deux si vous avez les deux disponibles.</li>
            "
            chain1="Nari 2.4 > Kanna 1.0 > FK 1.1 > CA"
            chain2="Eunha 1.0"
            chain3="Nari 1.3 > Kanna 0.2 > FK 1.2 > CA"
            chain4=""
            chain5=""
            chainSpaceship1=""
            chainSpaceship2=""
            chainSpaceship3=""
            chainSpaceship4=""
            chainSpaceship5=""
          />

          <Team onVideoPlay={onVideoPlay}
            heroes={["euh","nar","kan","lah"]}
            weapons={["euh","nar","kan","lah"]}
            dmg="49" cards={["skill-skill","crit-atk7","atk5-atk5","crit-crit"]}
            access={["mino","sg","sg","sg"]}
            video="https://www.youtube.com/watch?v=riXa9R4_Ooo"
            videoSpaceship=""
            relic="cup"
            pattern="1"
            warning="
            <li>Ne pas utiliser la marque d'Euhna (Attaque normale secondaire).</li>
            <li>Retarder la 2ème CA jusqu'à ce qu'il reste 40 secondes (-20 secondes après le début du combat). Le boss essaiera de se téléporter pour les orbes vers 33 et 6 secondes de la fin du combat (-17 secondes et -54 secondes). Assurez-vous que la chaîne de Kanna touche après que le statut Aéroporté se soit dissipé (<0.8) pendant la première chaîne, car elle l'annulera.</li>
            <li>Il faut se tenir à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'il donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
            <li>S'assurer que Nari et Lahn passent le plus de temps possible à frapper le boss est crucial pour maximiser les dégâts.</li>
            "
            chain1="(Attendre avant de stun pour cancel son pattern, voir Infos) Nari 0.1 > Kanna 0.6 > Eunha 1.5 > CA > Lahn 1.5"
            chain2="Kanna 0.7 > Nari 1.1 > Lahn 2.4 > Eunha 2.4 > CA"
            chain3=""
            chain4=""
            chain5=""
            chainSpaceship1=""
            chainSpaceship2=""
            chainSpaceship3=""
            chainSpaceship4=""
            chainSpaceship5=""
          />

          <Team onVideoPlay={onVideoPlay}
            heroes={["and","nar","kan","fk"]}
            weapons={["ido","nar","kan","fk"]}
            access={["sg","sg","sg","sg"]}      
            dmg="" cards={["crit-crit","atk5-atk5","atk5-atk5","crit-atk7"]}
            video=""
            videoSpaceship=""
            relic="cup"
            pattern="1"
            warning="
            <li>Pour annuler sa téléportation et son orbe d'énergie, n'oubliez pas d'attendre de le stun la première fois à 24-26 secondes lorsque le combat commence (vers 34-36 secondes sur le timer restant pour une durée d'attaque complète).</li>
            <li>Il faut se tenir à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'il donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
            "
            chain1="(Attendre avant de stun pour cancel son pattern, voir Infos)"
            chain2=""
            chain3=""
            chain4=""
            chain5=""
            chainSpaceship1=""
            chainSpaceship2=""
            chainSpaceship3=""
            chainSpaceship4=""
            chainSpaceship5=""
          />

          <Team onVideoPlay={onVideoPlay} 
            heroes={["euh","kni","kan","nar"]}
            weapons={["euh","kni","kan","nar"]}
            dmg="" cards={["skill-skill","crit-crit","atk5-atk5","crit-atk7"]}
            access={["mino","sg","sg","sg"]}      
            video="https://www.youtube.com/watch?v=eVQ3AwnUZhQ"
            videoSpaceship=""
            relic="cup"
            pattern="1"
            warning="
            <li>Pour annuler sa téléportation et son orbe d'énergie, n'oubliez pas d'attendre de le stun la première fois à 29-30 secondes lorsque le combat commence (vers 39-40 secondes sur le timer restant pour une attaque complète).</li>
            <li>Ne pas utiliser la marque d'Euhna (Attaque normale secondaire).</li>
            "
            chain1="(Attendre avant de stun pour cancel son pattern, voir Infos) Nari (0,2) > Kanna (0,2) > Eunha (1.5) > CA > Chevalier (0,1)"
            chain2="Nari (0,2) > Kanna (0,2) > Eunha (1.5) > CA > Chevalier (0,1)"
            chain3=""
            chain4=""
            chain5=""
            chainSpaceship1=""
            chainSpaceship2=""
            chainSpaceship3=""
            chainSpaceship4=""
            chainSpaceship5=""
          />    

          <Team onVideoPlay={onVideoPlay} 
            heroes={["and","nar","kan","ros"]}
            weapons={["ido","nar","kan","basic"]}
            access={["sg","sg","sg","sg"]}      
            dmg="" cards={["crit-crit","crit-atk7","atk5-atk5","atk5-atk5"]}
            video="https://www.youtube.com/watch?v=mupcr8rh4pQ"
            videoSpaceship=""
            relic="cup"
            pattern="1"
            warning="
            <li>Pour annuler sa téléportation et son orbe d'énergie, n'oubliez pas d'attendre de le stun la première fois à 24-26 secondes lorsque le combat commence (vers 34-36 secondes sur le timer restant pour une durée d'attaque complète).</li>
            <li>Il faut se tenir à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'il donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
            <li>Le collier minotaure est utilisé comme accessoire dans la vidéo, mais comme il n'y a pas besoin de la régénération de la compétence d'arme, il serait préférable d'utiliser les lunettes à la place.</li>
            "
            chain1="(Attendre avant de stun pour cancel son pattern, voir Infos) Kanna 0.6 > Nari 0.8"
            chain2="Kanna 0.6 > Nari 1.1"
            chain3=""
            chain4=""
            chain5=""
            chainSpaceship1=""
            chainSpaceship2=""
            chainSpaceship3=""
            chainSpaceship4=""
            chainSpaceship5=""
          />
    </>
  );
}