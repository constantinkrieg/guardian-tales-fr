import React from "react";
import Team from "../../Team";
 
export default function Dark({ onVideoPlay }) {
  return (
    <>
    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","chu","gab","tin"]}
      weapons={["and","chu","gab","light"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="47.3" cards={["crit-crit","atk5-atk5","atk5-light","crit-atk7"]}    
      video="https://www.youtube.com/watch?v=wISu_ApThCQ"
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning="
      <li>Utilisez votre première compétence d'arme en faisant un cycle d'attaque normale supplémentaires avant, afin que la marque de Tinia soit mieux alignée pour la chaîne.
      <li>Pendant la 1ère chaîne, Tinia annule la téléportation du boss</li>
      "
      chain1="Andras 1.4 > Chun 1.4 > CA > Gabrielle 1.3 > Tinia 0.7"
      chain2="Andras 1.4 > Chun 1.4 > CA > Gabrielle 1.3 > Tinia 2.4"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "tin", "gab"]}
        weapons={["and", "chu", "light", "gab"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45.7" cards={["crit-crit", "atk5-atk5", "crit-light", "atk5-atk7"]}
        video="https://www.youtube.com/watch?v=FwkASU5vpzc&t=6s"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""        
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","chu","gab","cam"]}
      weapons={["and","chu","gab","cam"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="43.3" cards={["atk7-atk5","atk5-atk5","atk5-light","atk5-dark"]}    
      video="https://www.youtube.com/watch?v=N4gabZJs30w"
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning="
      <li>Tenez-vous à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'elle donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
      <li>Assurez-vous que vos alliés passent le plus de temps possible à frapper le boss afin de maximiser vos dégâts.
      "
      chain1="Andras 1.5 > Chun 0.3 > CA > Gabrielle 1.0 > Cammie 2.5"   
      chain2="Andras 1.5 > Chun 0.1 > CA > Gabrielle 1.0 > Cammie 2.5"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    {/* 
    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","chu","ros","gab"]}
      weapons={["eva","chu","light","gab"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","crit-light","atk5-atk5","atk7-atk5"]}    
      video="https://www.youtube.com/watch?v=hx6hY5rCWeo"
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning="
      <li>Tenez-vous à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'elle donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
      <li>Assurez-vous que vos alliés passent le plus de temps possible à frapper le boss afin de maximiser vos dégâts.
      <li>Pendant les deux chaînes, Rosetta annule la téléportation (vous pouvez utiliser la chaîne de Rosetta plus tôt pendant la deuxième chaîne pour utiliser pleinement le statut aéroporté).
      "
      chain1="Andras 1.4 > Chun 1.1 > Gabrielle 1.3 > Rosetta 0.8"
      chain2="Andras 1.4 > Chun 1.4 > Gabrielle 1.3 > Rosetta 0.8"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />
  */}

  <Team onVideoPlay={onVideoPlay} 
      heroes={["kai","sha","val","lap"]}
      weapons={["kai","sha","val","lap"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="42.4" cards={["skill-skill","crit-crit","atk5-light","crit-atk7"]}    
      video="https://www.youtube.com/watch?v=0b_v1k00nlQ"
      videoSpaceship=""
      relic="cup"
      pattern="1"
      warning="
      <li>Le choix entre Kai et Valencia pour lead semble être aussi bien l'un que l'autre, essayez les deux versions et voyez laquelle vous convient le mieux.</li>
      <li>Faites en sorte que le boss aille vers le mur en vous plaçant à sa gauche. Ne vous placez pas à la même hauteur que lui, sinon les coups de poing vous atteindront. En vous plaçant entre le boss et le mur, il se retrouve coincé.</li>
      <li>Pendant les chaînes, c'est Shapira qui annule la téléportation du boss</li>
      "
      chain1="Valencia 1.4 > Lapice 0.1 > CA > Kai 1.4 > Shapira 0.9 > CA"
      chain2="Valencia 1.4 > Lapice 0.1 > CA > Kai 1.4 > Shapira 0.9"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["val","sha","kai","lap"]}
      weapons={["val","sha","kai","lap"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="42.3" cards={["skill-skill","crit-light","crit-crit","atk5-atk7"]}    
      video="https://www.youtube.com/watch?v=YJ5FanZ8CdI"
      videoSpaceship=""
      relic="cup"
      pattern="1"
      warning="
      <li>Le choix entre Kai et Valencia pour lead semble être aussi bien l'un que l'autre, essayez les deux versions et voyez laquelle vous convient le mieux.</li>
      <li>Faites en sorte que le boss aille vers le mur en vous plaçant à sa gauche. Ne vous placez pas à la même hauteur que lui, sinon les coups de poing vous atteindront. En vous plaçant entre le boss et le mur, il se retrouve coincé.</li>
      <li>Pendant les chaînes, c'est Shapira qui annule la téléportation du boss</li>
      "
      chain1="Valencia 1.4 > Lapice 1.7 > CA > Kai 1.5 > Shapira 0.9"
      chain2="Valencia 1.4 > Lapice 2.0 > CA > Kai 1.5 > Shapira 0.9"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay}
        heroes={["val", "lap", "ang", "sha"]}
        weapons={["va;", "lap", "ang", "sha"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="40.5" cards={["skill-skill", "crit-light", "crit-atk5", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=FwkASU5vpzc&t=6s"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""        
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

     {/* 
    <Team onVideoPlay={onVideoPlay} 
      heroes={["and","nar","ros","gab"]}
      weapons={["eva","light","light","gab"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-light","atk5-atk5","atk7-crit"]}    
      video="https://youtu.be/h72Lcr3BAcc?t=87"
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning="
      <li>Tenez-vous à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'elle donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
      <li>Assurez-vous que vos alliés passent le plus de temps possible à frapper le boss afin de maximiser vos dégâts.
      <li>Pendant les deux chaînes, Rosetta annule la téléportation (vous pouvez utiliser la chaîne de Rosetta plus tôt pendant la deuxième chaîne pour utiliser pleinement le statut aéroporté).
      <li>La team avec Tinia fait normalement plus de dégâts que Rosetta mais avec l'IA de Tinia, il y a un gros facteurs chances pour qu'elle soit meilleure. Nous recommandons plutôt d'utiliser Rosetta mais à vous de tester ce que vous préférez.</li>
      <li>Il y a une variante si vous n'avez pas l'arme d'Eva MLB (Rupture de limite maximum), vous pouvez jouer avec le bâton de Princesse du Futur (Libérateur). Les chaînes seront différentes cependant, <a target='_blank' href='https://youtu.be/h72Lcr3BAcc?t=173'><u>accédez à la vidéo en cliquant ici.</u></a>
      "
      chain1="Andras 1.4 > Nari 1.1 > Gabrielle 1.3 > Rosetta 0.8"
      chain2="Andras 1.4 > Nari 1.4 > Gabrielle 1.3 > Rosetta 0.8"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />
 */}
 
  <Team onVideoPlay={onVideoPlay} 
      heroes={["and","nar","tin","gab"]}
      weapons={["and","light","light","gab"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["crit-crit","atk5-light","atk5-atk5","atk7-crit"]}    
      video="https://youtu.be/h72Lcr3BAcc"
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning="
      <li>Tenez-vous à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'elle donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
      <li>Assurez-vous que vos alliés passent le plus de temps possible à frapper le boss afin de maximiser vos dégâts.
      <li>Pendant les deux chaînes, Rosetta annule la téléportation (vous pouvez utiliser la chaîne de Rosetta plus tôt pendant la deuxième chaîne pour utiliser pleinement le statut aéroporté).
      "
      chain1="Andras 1.4 > Nari 1.4 > CA > Gabrielle 1.3 > Tinia 0.7"
      chain2="Andras 1.4 > Nari 1.4 > CA > Gabrielle 1.3 > Tinia 2.4"
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["chu","gab","ele","euh"]}
      weapons={["chu","gab","ele","light"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["skill-skill","crit-light","crit-atk5","atk7-crit"]}    
      video="https://www.youtube.com/watch?v=34sq9tfSKsY"
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning="
      <li>Tenez-vous à un angle de 45° ou un peu plus haut pour que le boss utilise les cônes dans une direction où ils ne touchent pas vos alliés ou qu'elle donne des coups de poing de côté, auquel cas vous n'aurez qu'à vous repositionner légèrement.</li>
      <li>Pour une meilleure application de la réduction de défense, faites en sorte que Chun utilise sa compétences d'arme / sa compétence en chaîne ainsi que sa compétence additionel pendant qu'il recharge son attaque normale.</li>
      <li>Pendant la 1ère chaîne, c'est Gabrielle qui annule la téléportation du boss</li>
      "
      chain1="Eunha 0.7 > Eleanor 0.1 > Chun 0.5 > CA > Gabrielle 0.9"
      chain2="(Stun / Pas de chaîne)"
      chain3="Eunha 1.4 > Eleanor 1.2 > Chun 2.1 > CA > Gabrielle 1.3"
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />
      <Team onVideoPlay={onVideoPlay} 
      heroes={["chu","gab","ele","tin"]}
      weapons={["chu","gab","ele","light"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["skill-skill","crit-light","crit-atk5","atk7-crit"]}    
      video=""
      videoSpaceship=""
      relic="book"
      pattern="1"
      warning=""
      chain1=""
      chain2=""
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />

    <Team onVideoPlay={onVideoPlay} 
      heroes={["yuz","val","lil","rey"]}
      weapons={["yuz","val","lil","light"]}
      access={["mino", "sg", "sg", "sg"]}
      dmg="" cards={["skill-skill","crit-crit","crit-dark","atk7-light"]}    
      video="https://youtu.be/29ozbxpwVU8"
      videoSpaceship=""
      relic="cup"
      pattern="1"
      warning=""
      chain1=""
      chain2=""
      chain3=""
      chain4=""
      chain5=""
      chainSpaceship1=""
      chainSpaceship2=""
      chainSpaceship3=""
      chainSpaceship4=""
      chainSpaceship5=""
    />
</>
  );
}