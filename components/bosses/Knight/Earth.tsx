import React from "react";
import Team from "../../Team";

export default function Earth() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "cam", "pli"]}
        weapons={["dol", "nar", "cam", "pli"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="44.9" cards={["crit-crit", "atk5-atk7", "atk5-dark", "crit-fire"]}
        video="https://www.youtube.com/watch?v=KFuovQSBiS0"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1="Nari 0.6 > Andras 1.4 > Plitvice 0.8 > Compétence d'arme"
        chain2="Nari 0.6 > Andras 1.4 > Plitvice 0.8 > Compétence d'arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />         

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "cam", "pli"]}
        weapons={["dol", "chu", "cam", "pli"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk7-light", "atk5-dark", "crit-fire"]}
        video=""
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Chun 0.2 > Andras 1.2 > Plitvice 2.4 > Compétence d'arme"
        chain2="Chun 0.2 > Andras 1.2 > Plitvice 2.4 > Compétence d'arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  


      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "kam", "cam", "pli"]}
        weapons={["dol", "kam", "cam", "pli"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "akt5-earth", "atk7-dark", "crit-fire"]}
        video="https://youtu.be/BNrvKDRannw"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Les mouvements du boss sont réguliers, tant que vous vous positionnez correctement, vous pouvez le contrôler.</li>
        <li><b>Si vous avez du mal avec le début du combat, vous pouvez tenter de jouer <u><a href='https://youtu.be/6EIlsePgQN4' target='_blank'>de cette manière</a></u>
        <li>Il est possible de rester bloqué dans le boss lorsque l'on se trouve dans la moitié supérieure du contour rouge de la hitbox du dash, <u><a href='https://youtu.be/RVlYFnnL-24?si=lFLROp_UujT520YS' target='_blank'>comme on peut le voir ici.</a></u></li>
        "
        chain1="Andras 1.2 > Plitvice 1.2 > Kamael 1.2 > CA > Cammie 2.4"
        chain2="Andras 1.2 > Plitvice 1.2 > Kamael 1.2 > CA > Cammie 2.4"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["rey", "sci", "win", "lil"]}
        weapons={["rey", "sci", "win", "lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="41.2" cards={["skill-skill", "crit-crit", "crit-fire", "atk7-dark"]}
        video="https://www.youtube.com/watch?v=MH8jQwMcy0Q"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  
      
      <Team onVideoPlay={onVideoPlay}
        heroes={["elv", "euh", "luc", "vis"]}
        weapons={["elv", "fire", "luc", "vis"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-atk5", "atk5-fire", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=g0-Jkbo6pWo"
        videoSpaceship="https://www.youtube.com/watch?v=yGtPU8MDeJo"
        relic="cup"
        pattern="1"
        warning="
        <li><b>Si le niveau du boss est inférieur à 86, les teams Elvira sont plus puissantes dû à son passif. 🔻<br/>Après ce seuil plus le niveau du boss est élevé, moins Elvira fera de dégâts, et sera donc dépasser par d'autres équipes.</b></li>
        <li>Si vous stun le boss  avec le 1er coup de la 2ème compétence d'arme (Vidéo 1), utilisez la chaîne de Lucy à 0.1, 1.1 si vous assommez avec le 2ème coup. (Vidéo 2)</li>
        <li>La chaîne de Vishuvac est utilisée après qu'elle ait utilisé sa compétence alternative.</li>
        <li>Pousser le boss vers l'arrière permet de faire toucher la compétence d'arme une troisième fois.</li>"
        chain1="Lucy 1.1 > Eunha 1.1 > Vishuvac 1.7 > Compétence d'arme > Elvira 2.2"
        chain2="Lucy 1.1 > Eunha 1.1 > Vishuvac 2.1 > Compétence d'arme > Elvira 1.7"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      /> 

      <Team onVideoPlay={onVideoPlay}
        heroes={["elv", "vis", "kam", "cam"]}
        weapons={["elv", "vis", "kam", "cam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "crit-fire", "atk5-earth", "atk7-dark"]}
        video="https://www.youtube.com/watch?v=mHIPc7P8zjc"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>Si le niveau du boss est inférieur à 86, les teams Elvira sont plus puissantes dû à son passif. 🔻<br/>Après ce seuil plus le niveau du boss est élevé, moins Elvira fera de dégâts, et sera donc dépasser par d'autres équipes.</b></li>
        <li>Comme d'habitude, il est important de jouer avec le timing de la compétence additionel de Vishuvac</li>
        <li>Le positionnement est important pour pouvoir contrôler le mouvement du boss aussi bien que cela est fait dans la vidéo.</li>
        "
        chain1="Kamael 1.1 > Cammie 1.1 > Vishuvac 2.4 > CA > Elvira 2.0"
        chain2="Kamael 1.3 > Cammie 0.9 > Vishvac 2.0 > CA > Elvira 2.0"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      /> 

      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "lah"]}
        weapons={["euh", "nar", "kan", "lah"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video="https://www.youtube.com/watch?v=b0uRIDSSeeI"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        <li>La seconde compétence d'arme est utilisé 19.5 secondes après le début du combat. (à la moitié de la canalisation de son coup de marteau)</li>
        "
        chain1="Nari 0.3 > Kanna 0.6 > Eunha 1.5 > CA > Lahn 1.5"
        chain2="Nari 0.3 > Kanna 0.6 > Eunha 1.5 > CA > Lahn 1.5"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      /> 

      <Team onVideoPlay={onVideoPlay}
        heroes={["rey", "lil", "yuz", "sci"]}
        weapons={["rey", "lil", "yuz", "sci"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-dark", "crit-fire", "crit-crit"]}
        video="https://youtu.be/1pezfOt9w50"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Le fait d'avoir Scintilla en 3ème ou 4ème position semble rendre le début du combat plus régulier.</li>
        "
        chain1="Rey 1.0 > CA > Scintilla 1.1 > Lilith 0.5 > CA > Yuze 1.2"
        chain2="Rey 1.0 > CA > Scintilla 1.1 > Lilith 0.5 > CA > Yuze 1.2"
        chain3="Rey 1.0"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />                        
    </>
  );
}
