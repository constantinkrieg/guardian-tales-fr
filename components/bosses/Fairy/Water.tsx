import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <> 
        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","kam","cam","ame"]}
        weapons={["kam","earth","cam","ame"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","atk5-earth","atk5-dark","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=AEvDtkC-qWY"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        
        <li>Si vous pouvez de préférence, avoir un deuxième bâton exclusif pour le mettre sur Kamaël, autrement mettez-lui un bâton Terre. Mais Andras doit garder l'arme de Kamaël (Équinoxe) pour la compétence d'arme.</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Crit pour Andras</li>
        <li>Vous pouvez essayer également ces chaînes si vous préferez cette méthode <u><a href='https://www.youtube.com/watch?v=ri7t7wDGc78' target='_blank'>(Voir la vidéo)</a></u>.
        "
        chain1="Kamael 1.1 > Cammie 1.8 > Ameris 1.3 > CA"
        chain2="Andras 1.4"
        chain3="Kamael 1.1 > Cammie 2.1 > Ameris 1.3 > CA > Andras 0.7"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["kam","and","ame","cam"]}
        weapons={["kam","earth","ame","cam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","crit-earth","atk7-dark"]}    
        video="https://www.youtube.com/watch?v=NucGPB0p9dE"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Dégâts de compétence pour Kamael</li>
        <li>Vous pouvez essayer également ces chaînes si vous préferez cette méthode <u><a href='https://www.youtube.com/watch?v=oFiqJYTg3wI' target='_blank'>(Voir la vidéo)</a></u>.
        "
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />

        <Team onVideoPlay={onVideoPlay} 
        heroes={["kam","euh","may","ame"]}
        weapons={["kam","earth","may","ame"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk5-earth","crit-crit","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=22PpArAupkk"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>De préférence utilisez des lunettes comme accessoire pour Kamael, mais si vous avez du mal à empêcher la téléportation de la fée avant la 2ème chaîne, vous pouvez utiliser un collier Mino sur Kamael pour la vitesse de recharge de la compétence d'arme.</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Dégâts de compétence pour Kamael</li>
        "
        chain1="Ameris 1.3 > Mayreel 0.9 > CA"
        chain2="Kamael 1.1"
        chain3="Ameris 1.3 > Mayreel 0.1 > CA"
        chain4="Kamael 1.1 > Euhna 1.1 > CA"
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />

        <Team onVideoPlay={onVideoPlay} 
        heroes={["kam","may","tin","ame"]}
        weapons={["kam","may","tin","ame"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","atk5-earth","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=dVF-SZ38t5c"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>De préférence utilisez des lunettes comme accessoire pour Kamael, mais si vous avez du mal à empêcher la téléportation de la fée avant la 2ème chaîne, vous pouvez utiliser un collier Mino sur Kamael pour la vitesse de recharge de la compétence d'arme.</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Dégâts de compétence pour Kamael</li>
        "
        chain1="Ameris 1.3 > Mayreel 0.9 > CA"
        chain2="Kamael 1.1 > Tinia 2.4"
        chain3="Ameris 1.4 > Mayreel 0.1 > CA"
        chain4="Kamael 1.1 > Tinia 1.1 > CA"
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />

       
        <Team onVideoPlay={onVideoPlay} 
        heroes={["tin","and","ame","kam"]}
        weapons={["aob","earth","ame","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","crit-atk7","atk5-earth"]}    
        video="https://www.youtube.com/watch?v=ciS42-OnPjc"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Penser à utiliser sur Tinia un collier Miroir Terre avec le maximum en vitesse de recharge de compétence d'arme (y compris sur son arme)</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Pensez à coller le boss avant d'utiliser votre 2ème compétence d'arme, autrement vous risquez d'être cancel par son dash</li>
        "
        chain1="Kamael 1.1 > Tinia 1.6 > Ameris 1.4"
        chain2="Andras 1.4"
        chain3="Kamael 1.3 > Tinia 1.4 > Ameris 1.4 > CA > Andras 0.3"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />   

        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","ame","may","kam"]}
        weapons={["kam","ame","may","earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","crit-atk7","at5-earth","atk5-atk5"]}    
        video="https://www.youtube.com/watch?v=UC0DW1Ebmng"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        
        <li>Si vous pouvez de préférence, avoir un deuxième bâton exclusif pour le mettre sur Kamaël, autrement mettez-lui un bâton Terre. Mais Andras doit garder l'arme de Kamaël (Équinoxe) pour la compétence d'arme.</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Crit pour Andras</li>
        "
        chain1="Ameris 1.4 > Andras 1.4 > CA > Mayreel 1.0"
        chain2="Kamael 1.1"
        chain3="Ameris 1.4 > Andras 1.0 > CA > Mayreel 1.0"
        chain4="Kamael 1.1"
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","may","euh","kam"]}
        weapons={["ame","may","earth","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","atk5-earth","atk5-atk7"]}    
        video="https://www.youtube.com/watch?v=sN5HgBlYkNI"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Penser à utiliser sur Ameris un collier Miroir Terre avec le maximum en vitesse de recharge de compétence d'arme (y compris sur son arme)</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Dégâts de compétence pour Ameris</li>       
        "
        chain1="Ameris 1.6 > Eunha 1.0 > CA > Kamael 0.1"
        chain2="Ameris 1.4 > Eunha 1.0 > CA > Kamael 0.1"
        chain3="Ameris 1.3"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />


        <Team onVideoPlay={onVideoPlay} 
        heroes={["tin","ame","may","kam"]}
        weapons={["aob","ame","may","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","crit-earth","atk5-atk7"]}    
        video="https://www.youtube.com/watch?v=peZGE_c3o_M"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Penser à utiliser sur Tinia un collier Miroir Terre avec le maximum en vitesse de recharge de compétence d'arme (y compris sur son arme)</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Pensez à coller le boss avant d'utiliser votre 2ème compétence d'arme, autrement vous risquez d'être cancel par son dash</li>
        "
        chain1="Ameris 1.7 > Mayreel 0.5 > Tinia 2.4 > CA > Kamael 1.1"
        chain2="Ameris 1.5 > Mayreel 0.5 > Tinia 2.4 > CA > Kamael 1.1"
        chain3="Ameris 1.7"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />     

        <Team onVideoPlay={onVideoPlay}
        heroes={["ame","may","kam","tin"]}
        weapons={["ame","may","kam","tin"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","atk5-earth","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=uZucRC0QTBs"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Penser à utiliser sur Ameris un collier Miroir Terre avec le maximum en vitesse de recharge de compétence d'arme (y compris sur son arme)</li>
        <li>Beaucoup plus difficile à jouer que Tinia lead (voir ci-dessus) pour une augmentation mineure des dégâts. Il dépend aussi de la coopération de l'IA de Tinia, donc probablement moins de dégâts en moyenne, même s'il est bien exécuté.</li>
        <li>Bloquez les boules de feu pour que vos alliés ne soient pas interrompus.</li>
        <li>Favorisez une relique avec des Dégâts de compétence pour Ameris</li>       
        "
        chain1="Ameris 1.3 > Mayreel 0.1 > CA > Tinia 0.1 > Kamael 0.1"
        chain2="Ameris 1.3 > Mayreel 0.1 > CA > Tinia 0.1 > Kamael 0.1"
        chain3="Ameris 1.3"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

      <Team onVideoPlay={onVideoPlay} 
        heroes={["rue","rey","yuz","lil"]}
        weapons={["rue","earth","earth","lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-earth","crit-crit","atk7-atk5"]}    
        video="https://www.youtube.com/watch?v=ZBKohHQwSqk"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Penser à utiliser sur Rue un collier Miroir Terre avec le maximum en vitesse de recharge de compétence d'arme (y compris sur son arme)</li>
        <li>Bien que la chaîne de Yuze soit utilisée à 0.5 dans la vidéo, par moment le laser annule sa chaîne, il est donc plus sûr de l'utiliser plus tôt. A 0.9, la chaîne frappe parfois avant que les autres héros ne s'enfuient à cause du laser.</li>
        "
        chain1="Lilith 0.5 > Rue 1.6 > CA"
        chain2="Rey 0.5 > CA > Yuze 0.9"
        chain3="Lilith 0.1 > CA > Rue 0.7"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />                           
    </>
  );
}