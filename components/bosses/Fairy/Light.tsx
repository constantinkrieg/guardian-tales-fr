  import React from "react";
import Team from "../../Team";
 
export default function Light() {
  return (
    <>


        <Team onVideoPlay={onVideoPlay} 
        heroes={["euh","nar","kan","fk"]}
        weapons={["euh","nar","kan","fk"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="51.7" cards={["skill-skill","crit-atk7","atk5-atk5","crit-crit"]}    
        video="https://www.youtube.com/watch?v=wqlKmrcHTqo"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Utlisez la compétence d'arme pour stun le boss la première fois quand elle commence à préparer son laser (42 secondes restantes sur le timer - sur un combat complet)</li>
        <li>Bloquez les boules de feu pour qu'elles ne dérangent pas vos alliés Ne vous précipitez pas pour y parvenir. S'arrêter d'attaquer diminuera le bonus de dégâts critiques d'Eunha.</li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        "
        chain1="Nari 0.2 > Kanna 0.4 > FK 2.4 > CA"
        chain2="Eunha 1.0"
        chain3="Nari 2.4 > Kanna 0.5 > FK 0.5 > CA"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["euh","nar","kan","lah"] }
        weapons={["euh","nar","kan","lah"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="44.8" cards={["skill-skill","crit-atk7","atk5-atk5","crit-crit"]}    
        video="https://www.youtube.com/watch?v=E7dXurVvtT4"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Utlisez la compétence d'arme pour stun le boss la première fois quand elle commence à préparer son laser (42 secondes restantes sur le timer - sur un combat complet)</li>
        <li>Il faut stun pour la 2ème fois le boss lorsqu'il commencera à préparer son dash.</li>
        <li>Bloquez les boules de feu pour qu'elles ne dérangent pas vos alliés Ne vous précipitez pas pour y parvenir. S'arrêter d'attaquer diminuera le bonus de dégâts critiques d'Eunha.</li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        "
        chain1="Kanna 1.3 > Nari 1.4 > Lahn 1.6 > Euhna 2.3 > CA"
        chain2="Kanna 0.5 > Nari 1.4 > Lahn 1.6 > Euhna 2.3 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","nar","kan","fk"]}
        weapons={["ido","nar","kan","fk"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="42.2" cards={["crit-crit","atk5-atk5","atk5-atk5","crit-atk7"]}    
        video="https://streamable.com/19jgrw"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="Vous devez avoir le maximum en tant de réduction de compétence sur votre lead. (Arme / Accessoire)"
        chain1="Kanna 0.8 > Nari 1.1 > CA > Andras 0.6 > FK 1.5"
        chain2="Kanna 0.0~0.1 > Nari 1.1 > CA > Andras 0.7 > FK 1.2"
        chain3="Kanna 0.1 > Nari 1.1"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}