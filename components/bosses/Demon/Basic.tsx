import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>


        <Team onVideoPlay={onVideoPlay} 
        heroes={["ogh","vin","cla","cam"]}
        weapons={["ogh","vin","cla","cam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="50.2" cards={["skill-skill","atk7-atk5","crit-crit","crit-dark"]}    
        video="https://youtu.be/24CZGQoyruo"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Kitez (Attirer et déplacer) le boss pendant le combat en longeant le bord pour retarder ses attaques.</li>
        <li>Utiliser les drones d'Oghma de manière à bénéficier du bonus de dégâts qu'ils apportent pendant les compétences d'Oghma.</li>
        <li>Pendant le kiting, gardez le boss à une certaine distance de vos alliés, afin qu'ils ne soient pas renvoyés par l'une de ses attaques. Veillez également à ce qu'il n'utilise pas l'attaque en cône dans leur direction</li>
        "
        chain1="Vinette 0.9 > Claude 0.9 > CA > Oghma 0.3 > Cammie 1.0 > CA"
        chain2="Vinette 0.9 > Claude 2.4 > CA > Oghma 1.0 > Cammie 1.0 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","ogh","cam","vin"]}
        weapons={["cla","ogh","cam","vin"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="44.8" cards={["skill-skill","crit-crit","crit-dark","atk7-atk5"]}    
        video="https://www.youtube.com/watch?v=fda57uDitso"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Cette team est bien si vous avez du mal avec le kiting mais elle fera moins de dégâts qu'avec Oghma en lead.</li>
        "
        chain1="Claude 1.5 > Oghma 0.7 > Cammie 1.9 > CA > Vinette 0.9"
        chain2="Claude 0.5 > Oghma 0.5 > Cammie 0.8 > CA > Vinette 0.9"
        chain3="Claude 1.2"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}