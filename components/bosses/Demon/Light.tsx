import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>


        <Team onVideoPlay={onVideoPlay} 
        heroes={["euh","nar","kan","fk"]}
        weapons={["euh","nar","kan","fk"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-crit","atk5-atk5","crit-crit"]}    
        video="https://youtu.be/stzKBQ6OoEw?si=CPXjvQG67n1vKjPF"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><h4><b>Ne mettez PAS de collier Mino sur Euhna, mettez à la place: Lunettes de Sniper ou Tireur affuté.</b></h4></li>
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        <li>Kitez (Attirer et déplacer) le boss au début du run pour retarder ses attaques. FK peut voler l'aggro, comme on peut le voir dans la vidéo.</li>
        <li>Pendant le kiting, gardez le boss à une certaine distance de FK et de Nari, afin qu'ils ne soient pas renvoyés par l'une de ses attaques. Veillez également à ce qu'il n'utilise pas l'attaque en cône dans leur direction</li>
        "

        chain1="Nari 0.1 > Kanna 0.2 > FK 1.1 > CA"
        chain2="Eunha 0.1"
        chain3="Nari 1.5 > Kanna 0.5 > FK 1.5 > CA"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["euh","nar","kan","lah"]}
        weapons={["euh","nar","kan","lah"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-crit","atk5-atk5","crit-crit"]}    
        video="https://www.youtube.com/watch?v=WOgfq4l2TDE"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        <li>Kitez le boss tout au long du combat pour retarder les rochers. Approchez-vous de lui avant d'utiliser la 2ème compétence d'arme, pour qu'il commence à lancer l'attaque de rochers avant que vous ne l'étourdissiez.</li>
        <li>Pendant le kiting, gardez le boss à une certaine distance de Lahn et de Nari, afin qu'ils ne soient pas renvoyés par l'une de ses attaques. Veillez également à ce qu'il n'utilise pas l'attaque en cône dans leur direction</li>
        "
        chain1="Nari 0.5 > Kanna 0.5 > Eunha 0.7 > Compétence d'Arme > Lahn 1.4"           
        chain2="Nari 0.5 > Kanna 0.5 > Eunha 1.5 > Compétence d'Arme > Lahn 1.4"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","kan","nar","kni"]}
        weapons={["ido","kan","nar","kni"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","atk5-atk5","atk5-atk5","atk7-crit"]}    
        video="https://www.youtube.com/watch?v=19YgFJ7HNK8"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<li>Cette team provient d'un vieux raid (il y a 9 mois).</li>"
        chain1="Kanna 0.2 > Nari 1.1 > CA > Chevalier 0.2 > Andras 1.5"           
        chain2="Kanna 0.5 > Nari 1.1 > CA > Chevalier 0.2 > Andras 1.0"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}