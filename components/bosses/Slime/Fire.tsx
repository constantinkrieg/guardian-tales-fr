import React from "react";
import Team from "../../Team";

export default function Teams({ onVideoPlay }) {
    return (
    <>      
    <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "cam", "orc"]}
        weapons={["and", "nar", "cam", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "atk5-dark", "crit-crit"]}
        video="https://youtu.be/WhZq3BUOYEo"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Andras 2.0 > Nari 1.6 > CA > Orca 0.7"
        chain2="Andras 1.5 > Nari 1.6 > CA > Orca 0.7"
        chain3="Andras 0.3"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "cam", "orc"]}
        weapons={["and", "chu", "cam", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="49.8" cards={["skill-skill", "atk7-light", "atk5-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=Bxj8I-NeZX0"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Ne restez pas immobile dans les flaques de lave, car elles font beaucoup de dégâts.</li>
        <li>Retarde la compétence d'arme avant la 2ème chaîne avec un cycle d'attaques normales</li>"        
        chain1="Andras 2.0 > Chun 1.6 > CA > Orca 0.7"
        chain2="Andras 1.5 > Chun 1.6 > CA > Orca 0.7"
        chain3="Andras 0.3"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      /> 

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "gar", "kam", "tin"]}
        weapons={["and", "gar", "water", "water"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="46.6" cards={["skill-skill", "crit-crit", "atk5-atk5", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=VAxkm-ibTiM"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""        
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
      
    <Team onVideoPlay={onVideoPlay}
        heroes={["and", "kam", "cam", "orc"]}
        weapons={["and", "water", "cam", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45.8" cards={["crit-crit", "atk5-atk5", "atk5-dark", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=hNDLDJbFsIU"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>La chaîne d'Andras annule le saut</li>
        "        
        chain1="Kamael 1.2 > Cammie 1.5 > CA > Orca 0.7 > Andras 1.0"
        chain2="Kamael 1.2 > Cammie 1.5 > CA > Orca 0.7 > Andras 1.0"
        chain3="Kamael 1.1"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["yuz", "whi", "ang", "rey"]}
        weapons={["yuz", "whi", "ang", "water"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45.3" cards={["skill-skill", "crit-atk7", "crit-atk5", "crit-atk5"]}
        video="https://www.youtube.com/watch?v=kvDnc2CKXg0"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""        
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />


      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "tin", "gab"]}
        weapons={["and", "chu", "water", "gab"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="43.7" cards={["skill-skill", "atk5-atk7", "crit-crit", "crit-light"]}
        video="https://www.youtube.com/watch?v=gqt5LJlt5Uo"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""        
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "orc", "gar", "nar"]}
        weapons={["and", "orc", "gar", "water"]}
        access={["skill-skill", "crit-crit", "crit-atk7", "atk5-atk5"]}
        dmg="40.8" cards={["crit-crit", "atk5-atk5", "crit-atk7", "atk5-atk5"]}
        video="https://www.youtube.com/watch?v=nmPnuV3Fcvc"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Ne restez pas immobile dans les flaques de lave, car elles font beaucoup de dégâts.</li>
        <li>La chaîne de Garam annule le saut, vous pouvez l'utiliser n'importe où entre 0.7-1.2, mais l'utiliser autour de 0.7-0.8 vous permet d'utiliser toute la compétence d'arme sous Aéroporté.</li>
        "        
        chain1="Andras 1.4 > Nari 1.2 > CA > Orca 0.7 > Garam 0.7-0.9 > CA"
        chain2="Andras 1.4 > Nari 1.1 > CA > Orca 0.7 > Garam 0.7-0.8 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "orc", "gar", "chu"]}
        weapons={["and", "orc", "gar", "chu"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="40.7" cards={["skill-skill", "crit-crit", "crit-atk7", "atk5-light"]}
        video="https://www.youtube.com/watch?v=J7g1PptvYY4"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Ne restez pas immobile dans les flaques de lave, car elles font beaucoup de dégâts.</li>
        <li>La chaîne de Garam annule le saut, vous pouvez l'utiliser n'importe où entre 0.7-1.2, mais l'utiliser autour de 0.7-0.8 vous permet d'utiliser toute la compétence d'arme sous Aéroporté.</li>
        "        
        chain1="Andras 1.4 > Chun 1.2 > CA > Orca 0.7 > Garam 0.7-0.9 > CA"
        chain2="Andras 1.4 > Chun 1.1 > CA > Orca 0.7 > Garam 0.7-0.8 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

       <Team onVideoPlay={onVideoPlay}
        heroes={["yuz", "rey", "lil", "whi"]}
        weapons={["yuz", "water", "lil", "whi"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-atk7", "atk5-dark", "crit-crit"]}
        video="https://youtu.be/qOW04V9Mb0Y?t=175"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""        
        chain1="Yuze 1.1 > Neige Blanche 1.1 > CA > Lilith 1.1"
        chain2="Yuze 1.1 > Neige Blanche 1.1 > CA > Lilith 1.1"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />    
        
      <Team onVideoPlay={onVideoPlay}
        heroes={["ver", "kam", "gar", "orc"]}
        weapons={["ver", "water", "gar", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk5-atk5", "crit-crit", "crit-atk7"]}
        video="https://youtu.be/Zika83F9L_M"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""        
        chain1="Kamael 2.0 > Veronica 1.1 > CA > Garam 1.6 > Orca 0.9"
        chain2="Kamael 1.7 > Veronica 1.1 > CA > Garam 1.6 > Orca 0.8"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />               
    </>
  );
}
