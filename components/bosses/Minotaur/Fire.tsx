import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>
        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","nar","cam","orc"]}
        weapons={["and","nar","cam","orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-light","crit-dark","crit-crit"]}    
        video=""
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <b>Même principe qu'avec Chun sauf le timing des chaînes (voir la vidéo en dessous)</b></li>
        <li>Utilisez la compétence en chaine de Nari après son dernier coup d'attaque normale si possible afin de maximiser l'application de la réduction de défense</li>
        <li>Avant d'utiliser la 2ème compétence d'arme  Compétence d'Arme et celle qui étourdit le boss pour la 2ème fois, faites 1 coup d'attaque normale supplémentaire, cela vous aidera à aligner les timings.</li>
        <li>Kiter le boss avant la 2ème chaîne retardera son saut (le stun annulera le saut).</li>
        "
        chain1="Andras 1.5 > Nari 1.1 > Compétence d'Arme > Orca 0.7"           
        chain2="Andras 1.5 > Nari 1.1 > Compétence d'Arme > Orca 0.7"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","cam","orc"]}
        weapons={["and","chu","cam","orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="51.7" cards={["skill-skill","atk7-light","crit-dark","crit-crit"]}    
        video="https://youtu.be/lm5GDbdGsdw"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Utilisez la compétence en chaine de Chun après son dernier coup d'attaque normale si possible afin de maximiser l'application de la réduction de défense</li>
        <li>Kiter le boss avant la 2ème chaîne retardera son saut (le stun annulera le saut).</li>
        "
        chain1="Andras 1.5 > Chun 1.3 > Compétence d'Arme > Orca 0.7"           
        chain2="Andras 1.5 > Chun 1.1 > Compétence d'Arme > Orca 0.9"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 


        <Team onVideoPlay={onVideoPlay} 
          heroes={["and","kam","gar","tin"]}
          weapons={["and","water","gar","water"]}
          access={["mino", "sg", "sg", "sg"]}
          dmg="49.1" cards={["skill-skill","atk5-atk5","crit-crit","crit-atk7"]}    
          video="https://www.youtube.com/watch?v=iQZapr6Hqno"
          videoSpaceship=""
          relic="cup"
          pattern="1"
          warning=""
          chain1="Andras 1.4 > Garam 1.4 > CA > Kamael 1.2 > Tinia 1.2"
          chain2="Andras 1.4 > Garam 1.2~1.4 > CA > Kamael 0.5~0.6 > Tinia 1.0~1.2 > CA"
          chain3=""
          chain4=""
          chain5=""
          chainSpaceship1=""
          chainSpaceship2=""
          chainSpaceship3=""
          chainSpaceship4=""
          chainSpaceship5=""
        />


        <Team onVideoPlay={onVideoPlay} 
          heroes={["yuz","ang","rey","whi"]}
          weapons={["yuz","ang","water","whi"]}
          access={["mino", "sg", "sg", "sg"]}
          dmg="49" cards={["skill-skill","crit-crit","crit-atk5","crit-atk7"]}    
          video="https://www.youtube.com/watch?v=jCUjybZRTw8"
          videoSpaceship=""
          relic="book"
          pattern="1"
          warning=""
          chain1="Angie 1.2 > CA + Rey 0.1 > Yuze 1.1 > Neige 0.2~0.3"
          chain2="Angie 1.2 > CA + Rey 0.1 > Yuze 1.1 > Neige 0.2~0.3"
          chain3=""
          chain4=""
          chain5=""
          chainSpaceship1=""
          chainSpaceship2=""
          chainSpaceship3=""
          chainSpaceship4=""
          chainSpaceship5=""
        />

        <Team onVideoPlay={onVideoPlay} 
          heroes={["yuz","bar","ang","whi"]}
          weapons={["yuz","bar","ang","whi"]}
          access={["mino", "sg", "sg", "sg"]}
          dmg="45.7" cards={["skill-skill","atk5-earth","crit-crit","crit-atk7"]}    
          video="https://www.youtube.com/watch?v=5xXljzRkwno"
          videoSpaceship=""
          relic="book"
          pattern="1"
          warning=""
          chain1="Yuze 1.1 > Bari 0.5 + CA > Angie 1.0 > CA"
          chain2=""
          chain3=""
          chain4=""
          chain5=""
          chainSpaceship1=""
          chainSpaceship2=""
          chainSpaceship3=""
          chainSpaceship4=""
          chainSpaceship5=""
        />

        <Team onVideoPlay={onVideoPlay} 
          heroes={["and","chu","orc","tin"]}
          weapons={["and","chu","orc","water"]}
          access={["mino", "sg", "sg", "sg"]}
          dmg="43.8" cards={["crit-crit","atk5-light","crit-atk7","atk5-atk5"]}    
          video="https://www.youtube.com/watch?v=VmWSGRK8aAc"
          videoSpaceship=""
          relic="book"
          pattern="1"
          warning=""
          chain1="Andras 1.5 > Chun 1.5 > CA + Orca 0.8"
          chain2="Andras 1.5 > Chun 1.5 > CA + Orca 0.8"
          chain3=""
          chain4=""
          chain5=""
          chainSpaceship1=""
          chainSpaceship2=""
          chainSpaceship3=""
          chainSpaceship4=""
          chainSpaceship5=""
        />

        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","nar","gar","orc"]}
        weapons={["and","water","gar","orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk5-light","crit-atk7","crit-crit"]}    
        video="https://www.youtube.com/watch?v=k4HqPP1FOt0 "
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Utilisez la compétence en chaine de Nari après le dernier coup si possible, mais au plus tard à 1.0</li>
        <li>Avant d'utiliser la 2ème compétence d'arme et celle qui étourdit le boss pour la 2ème fois, faites 1 coup d'attaque normale supplémentaire, cela vous aidera à aligner les timings.</li>
        <li>Kiter le boss avant la 2ème chaîne retardera son saut (le stun annulera le saut).</li>
        "
        chain1="Andras 1.5 > Nari 1.4 > Compétence d'Arme > Orca 0.7 > Garam 2.4"           
        chain2="Andras 1.4 > Nari 1.3 > Compétence d'Arme > Orca 0.7 > Garam 0.5 > Compétence d'Arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","gar","orc"]}
        weapons={["and","chu","gar","orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="41.8" cards={["crit-crit","atk5-light","crit-atk7","atk5-atk5"]}    
        video="https://youtu.be/yVz51ta5dM4?t=4"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>Même principe qu'avec Nari sauf le timing des chaînes (voir la vidéo au dessus)</b></li>
        <li>Utilisez la compétence en chaine de Chun après le dernier coup si possible, mais au plus tard à 1.1</li>
        <li>Avant d'utiliser la 2ème compétence d'arme  et celle qui étourdit le boss pour la 2ème fois, faites 1 coup d'attaque normale supplémentaire, cela vous aidera à aligner les timings.</li>
        <li>Kiter le boss avant la 2ème chaîne retardera son saut (le stun annulera le saut).</li>
        "
        chain1="Andras 1.5 > Chun 1.4 > Compétence d'Arme > Orca 0.7 > Garam 2.4"           
        chain2="Andras 1.4 > Chun 1.3 > Compétence d'Arme > Orca 0.7 > Garam 0.5 > Compétence d'Arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["yuz","rey","lil","whi"]}
        weapons={["yuz","water","lil","whi"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="41.2" cards={["skill-skill","crit-atk7","atk5-dark","crit-crit"]}    
        video="https://youtu.be/sRfwSXXIb3s"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Si le boss a fait dans son pattern une attaque de plus par rapport au moment où vous deviez le stun la 2ème fois, utiliser la compétence en chaîne de Yuze à 1.1 au lieu de 0.9.  Cela peut arriver parce que vous avez été trop lent à l'étourdir avant la première chaîne (Neige Blanche annulera le saut à la place de Lilith). Il est possible que la chaîne de Neige Blanche loupe si elle est utilisée à 0.1. Vous pouvez l'utiliser à 0.2-0.3 pour être sûr) ou si vous êtes trop lent à l'étourdir avant la 2ème chaîne (le boss tournera la tête vers vous))</li>
        "
        chain1="Yuze 1.1 > Compétence d'Arme + Neige Blanche 0.1 > Lilith 0.3 > Compétence d'Arme"           
        chain2=" Yuze 0.9 > Compétence d'Arme + Neige Blanche 0.1 > Lilith 0.3 > Compétence d'Arme"
        chain3="Yuze 2.4"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}