import React from "react";
import Team from "../../Team";

export default function Tams({ onVideoPlay }) {  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "fk"]}
        weapons={["euh", "nar", "kan", "fk"]}
        access={["sg", "sg", "sg", "sg"]}
        dmg="55.4" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video=""
        videoSpaceship="https://www.youtube.com/watch?v=87v9fOWQxXM"
        relic="cup"
        pattern="harvester"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />   
      
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "fk"]}
        weapons={["euh", "nar", "kan", "fk"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="56" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video="https://www.youtube.com/watch?v=d8fga1d_iV4"
        videoSpaceship="https://www.youtube.com/watch?v=STLPukA1m2E"
        relic="cup"
        pattern="harvester"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />    
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "lah", "nar", "kan"]}
        weapons={["euh", "lah", "nar", "kan"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="50" cards={["skill-skill", "crit-crit", "crit-atk7", "atk5-atk5"]}
        video="https://www.youtube.com/watch?v=Q6KbsgUp5_w"
        videoSpaceship="https://www.youtube.com/watch?v=Y0UHAixrC08"
        relic="cup"
        pattern="harvester"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />        
      </>
    );
}