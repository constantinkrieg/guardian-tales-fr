import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "kam", "cam", "cla"]}
        weapons={["and", "kam", "cam", "cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-earth", "atk5-dark", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=C1JtHSYe6OE"
        videoSpaceship="https://www.youtube.com/watch?v=1G2yjlmMYo0"
        relic="book"
        pattern="2"
        warning="
      <ul>
      <li><b>Le pattern du Moissonneur change à 60% (au lieu de 50%).</b></li>
      <li>Le moissonneur se téléporte toujours dans le quadrant opposé à celui où se trouve Andras.</li>
      <li>Faire en sorte que l'attaque de la vague aille dans une direction différente de celle où se trouvent vos alliés.</li>
      <li>Si vous attaquez le moissonneur sur son pattern -60% HP, déplacez vous avec (kite) après la 1ère chaîne.</li>
      </ul>"
        chain1="Andras (0.1) > Claude (1.4) > CA > Kamael (1.1) > Cammie (1.3)"
        chain2="Andras (0.1) > Claude (1.4) > CA > Kamael (1.1) > Cammie (0.5) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1="Andras (0.1) > Claude (1.0) > CA > Kamael (1.1) > Cammie (0.1) > CA"
        chainSpaceship2="Andras (0.1) > Claude (1.0) > CA > Kamael (1.1) > Cammie (0.1) > CA"
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "nar", "cam", "and"]}
        weapons={["cla", "nar", "cam", "dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-dark", "crit-crit"]}
        video="https://youtu.be/-o-ff7EqCO4"
        videoSpaceship="https://youtu.be/8WB13ou9PGE"
        relic="book"
        pattern="2"
        warning="
    <ul>
    <li><b>Le pattern du Moissonneur change à 60% (au lieu de 50%).</b></li>
    <li>Le Moissonneur se téléporte toujours dans le quadrant opposé à celui où se trouve Claude. Faites-le se téléporter sur place/près de vos alliés, pour qu'Andras puisse continuer à frapper.</li>
    <li>Faites en sorte que la vague d'attaque aille dans une direction différente de celle où se trouvent vos alliés.</li>
    <li>Si vous attaquez le moissonneur sur son pattern -60% HP, déplacez-vous pour attirer le boss vers le centre lorsque Andras et Cammie utilisent leur compétence en chaîne.</li>
    </ul>"
        chain1="Claude (0.3) > Andras (1.1) > Nari (2.4) > CA"
        chain2="Claude (1.3) > Andras (1.1) > Nari (2.4) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1="Claude (1.4) > Andras (1.4) > Nari (1.4) > CA"
        chainSpaceship2="Claude (1.3) > Andras (1.1) > Nari (0.5) > CA"
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "kam", "cam", "and"]}
        weapons={["cla", "kam", "cam", "dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-earth", "crit-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=ezmuEGBjZtI"
        videoSpaceship="/static/videos/s56-harvester-basic.mp4"
        relic="book"
        pattern="2"
        warning="
    <ul>
    <li><b>Le pattern du Moissonneur change à 60% (au lieu de 50%).</b></li>
    <li>Le Moissonneur se téléporte toujours dans le quadrant opposé à celui où se trouve Claude. Faites-le se téléporter sur place/près de vos alliés, pour qu'Andras puisse continuer à frapper.</li>
    <li>Faites en sorte que la vague d'attaque aille dans une direction différente de celle où se trouvent vos alliés.</li>
    <li>Si vous attaquez le moissonneur sur son pattern -60% HP, dirigez le boss vers le centre après la 1ère chaîne.</li>
    </ul>"
        chain1="Claude (0.3) > Kamael (1.1) > Cammie (2.4) > CA"
        chain2="Claude (1.4) > Kamael (1.1) > Cammie (2.4) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1="Andras (0.1) > Claude (1.1) > CA > Kamael (1.3) > Cammie (0.1) > CA"
        chainSpaceship2="Andras (0.1) > Claude (1.1) > CA > Kamael (1.3) > Cammie (0.1) > CA"
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "ogh", "cam", "nar"]}
        weapons={["cla", "ogh", "cam", "nar"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-crit", "crit-dark", "atk7-atk5"]}
        video="https://www.youtube.com/watch?v=2iWvw0RBuAo"
        videoSpaceship="https://www.youtube.com/watch?v=DX8GqDclkcc"
        relic="book"
        pattern="2"
        warning="
    <ul>
    <li><b>Le pattern du Moissonneur change à 60% (au lieu de 50%).</b></li>
    <li>Le Moissonneur se téléporte toujours dans le quadrant opposé à celui où se trouve Claude. Faites-le se téléporter sur place/près de vos alliés, pour qu'Andras puisse continuer à frapper.</li>
    <li>Faire en sorte que l'attaque de la vague aille dans une direction différente de celle où se trouvent vos alliés.</li>
    </ul>"
        chain1="Claude (1.6) > Oghma (1.2) > Cammie (2.4) > CA"
        chain2="Claude (1.4) > Oghma (1.1) > Cammie (2.4) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1="Claude (1.6) > Oghma (1.4) > Cammie (2.4) > CA"
        chainSpaceship2="Claude (1.5) > Oghma (1.5) > Cammie (2.4) > CA"
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "ogh", "cam", "kam"]}
        weapons={["cla", "ogh", "cam", "kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-crit", "crit-dark", "atk7-earth"]}
        video="https://www.youtube.com/watch?v=Zmfl2PzyATE"
        videoSpaceship="https://www.youtube.com/watch?v=Zn9JJLvYIqs"
        relic="book"
        pattern="2"
        warning="
    <ul>
    <li><b>Le pattern du Moissonneur change à 60% (au lieu de 50%).</b></li>
    <li>Le Moissonneur se téléporte toujours dans le quadrant opposé à celui où se trouve Claude. Faites-le se téléporter sur place/près de vos alliés, pour qu'Andras puisse continuer à frapper.</li>
    <li>Faire en sorte que l'attaque de la vague aille dans une direction différente de celle où se trouvent vos alliés.</li>
    </ul>"
        chain1="Claude (1.2) > Oghma (1.2) > Cammie (2.4) > CA > Kamael (1.1)"
        chain2="Claude (1.2) > Oghma (1.2) > Cammie (2.4) > CA > Kamael (1.1)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1="Claude (1.2) > Oghma (1.2) > Cammie (2.4) > CA > Kamael (1.1)"
        chainSpaceship2="Claude (1.2) > Oghma (1.2) > Cammie (2.4) > CA > Kamael (1.1)"
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
