import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>

      <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","tin","and","kam"]}
        weapons={["ame","tin","earth","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk5-crit","crit-crit","atk7-atk5"]}    
        video="https://www.youtube.com/watch?v=aWZuWGGh6ao"
        videoSpaceship=""
        relic="cup"
        pattern="harvester"
        warning=""
        chain1="Kamael 1.1 > Tinia 0.6 > CA > Ameris 1.5"
        chain2="Kamael 1.1 > Tinia 0.6 > CA > Ameris 1.5"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","may","euh","kam"]}
        weapons={["ame","may","earth","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","atk5-earth","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=LycQzCUC9tU"
        videoSpaceship=""
        relic="cup"
        pattern="harvester"
        warning=""
        chain1="Ameris 1.5 > Euhna 0.1 > CA > Kamael 1.1"
        chain2="Ameris 1.5 > Euhna 0.1 > CA > Kamael 1.1"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","tin","may","kam"]}
        weapons={["ame","tin","may","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk5-crit","crit-earth","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=l1a6qp_gJTY"
        videoSpaceship=""
        relic="cup"
        pattern="harvester"
        warning=""
        chain1="Ameris 1.5 > Mayreel 1.5 > Tinia 2.4 > CA > Kamael 1.1"
        chain2="Ameris 1.5 > Mayreel 1.5 > Tinia 2.4 > CA > Kamael 1.1"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
   
    </>
  );
}
