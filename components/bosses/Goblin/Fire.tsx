import React from "react";
import Team from "../../Team";

export default function Fire() {
  return (
    <>


    <Team onVideoPlay={onVideoPlay}
        heroes={["and", "orc", "cam", "nar"]}
        weapons={["and", "orc", "cam", "nar"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.8" cards={["skill-skill", "crit-crit", "crit-dark", "atk5-atk7"]}
        video="https://youtu.be/0834Dzv02Jk"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>L'utilisation de Nari au lieu de Chun permet d'obtenir des dégâts plus réguliers, alors que Chun a des dégâts maximums plus élevés.</li>
        <li>Nari au lieu de Chun se joue de la même façon, vous pouvez utiliser sa chaîne un peu plus tard (1.1 ou plus).</li>
        "
        chain1="Andras 1.5 > Nari 1.1 > Compétence d'arme > Orca 0.7"
        chain2="Andras 1.5 > Nari 1.1 > Compétence d'arme > Orca 0.7"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  
      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "orc", "cam", "chu"]}
        weapons={["and", "orc", "cam", "chu"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="51.4" cards={["skill-skill", "crit-crit", "crit-dark", "atk7-light"]}
        video="https://www.youtube.com/watch?v=hBXagOVGqgQ"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>L'utilisation de Nari au lieu de Chun permet d'obtenir des dégâts plus réguliers, alors que Chun a des dégâts maximums plus élevés.</li>
        <li>La chaîne de Chun est utilisée après qu'il ait appliqué la réduction de défense avec son dernier coup.</li>
        <li>Il peut être intéressant de se faire toucher intentionnellement par le dash, car cela retarde un peu la 1ère chaîne, ce qui permet de mieux aligner le timing du def shred de Chun.</li>
        <li>L'endroit où Chef Gobelin lance les bombes autour de lui est affecté par votre position, mais cela ne semble pas être le seul facteur.</li>
        </ul>"        
        chain1="Andras 1.5 > Chun 1.5 > Compétence d'arme > Orca 0.7"
        chain2="Andras 1.5 > Chun 1.5 > Compétence d'arme > Orca 0.7"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      /> 

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "gar", "orc"]}
        weapons={["and", "water", "gar", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-atk5", "crit-atk7", "atk5-atk5"]}
        video="https://www.youtube.com/watch?v=8KUZMyS648w"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<ul>
        <li>L'utilisation de Nari au lieu de Chun permet d'obtenir des dégâts plus réguliers, alors que Chun a des dégâts maximums plus élevés.</li>
        </ul>"
        chain1="Andras (1.5) > Nari (1.5) > CA > Orca (0.6) > Garam (2.0)"
        chain2="Andras (1.5) > Nari (0.5) > CA > Orca (0.7) > Garam (0.7) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "gar", "orc"]}
        weapons={["and", "chu", "gar", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="42.6" cards={["skill-crit", "atk5-light", "crit-atk7", "crit-crit"]}
        video="https://www.youtube.com/watch?v=_iWXkZQP97E"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<ul>
        <li>L'utilisation de Nari au lieu de Chun permet d'obtenir des dégâts plus réguliers, alors que Chun a des dégâts maximums plus élevés.</li>
        </ul>"       
        chain1="Andras 1.5 > Chun 1.5 > Compétence d'arme > Orca 0.7 > Garam 2.4"
        chain2="Andras 1.5 > Chun 1.5 > Compétence d'arme > Orca 0.7 > Garam 0.4"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "kam", "cam", "orc"]}
        weapons={["and", "water", "cam", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-atk5", "atk5-dark", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=JAeQJXmZCHE"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""        
        chain1="Kamael (1.1) > Cammie (0.5) > CA > Orca (0.7) > Andras (1.5)"
        chain2="Kamael (1.1) > Cammie (0.5) > CA > Orca (0.7) > Andras (1.5)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "tin", "orc"]}
        weapons={["and", "chu", "water", "orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="42.6" cards={["skill-crit", "atk5-light", "crit-atk7", "crit-crit"]}
        video=""
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<ul>
        <li>L'utilisation de Nari au lieu de Chun permet d'obtenir des dégâts plus réguliers, alors que Chun a des dégâts maximums plus élevés.</li><
        <li>Faites attention à la façon dont le combat est joué (à quelle distance faire marcher le boss au début et le timing des chaînes), sinon Tinia peut manquer sa marque pendant le dash du gobelin ou la chaîne d'Orca va frapper sans que 2 marques soient appliquées.</li>
        </ul>"       
        chain1="Andras 1.6 > Chun 1.1 > Compétence d'arme > Orca 0.9"
        chain2="Andras 1.4 > Chun 1.0 > Compétence d'arme > Orca 1.0"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["yuz", "rey", "lil", "whi"]}
        weapons={["yuz", "water", "lil", "whi"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="38.5" cards={["skill-skill", "crit-atk7", "atk5-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=UNVhVRP9Wfc"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Au début, déplacez-vous vers le boss pour qu'il vous bombarde, après quoi vous pouvez amener le boss vers le haut et le bloquer pour le reste du combat.</li>
        </ul>"
        chain1="Yuze (1.1) > CA > Neige Blanche (0.1) > Rey (0.2) > CA"
        chain2="Yuze (1.1) > CA > Neige Blanche (0.1) > Rey (0.1)> CA"
        chain3="Yuze (2.1)"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />              
    </>
  );
}
