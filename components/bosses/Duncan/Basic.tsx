import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>
        <Team onVideoPlay={onVideoPlay} 
        heroes={["ogh","vin","cla","cam"]}
        weapons={["ogh","vin","cla","cam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.6" cards={["skill-skill","atk7-atk5","crit-crit","crit-dark"]}    
        video="https://www.youtube.com/watch?v=bGixejWlwmQ"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Utilisez la chaîne de Vinette après le dernier coup de sa séquence d'attaque normale, mais à 0.8 au plus tard et à 1.2 au plus tôt.</li>
        <li>Les dégâts maximaux de cette équipe sont plus élevés que ceux de Claude en lead, mais les dégâts ne sont pas aussi fiables. Le déroulé du combat est joué différemment maintenant, donc peut-être que ce n'est plus le cas. Essayez par vous-même !</li>
        "
        chain1="Vinette 0.8 > Claude 1.7 > Compétence d'arme > Oghma 1.1 > Cammie 0.6 > Compétence d'arme"           
        chain2="Vinette 1.2 > Claude 1.7 > Compétence d'arme > Oghma 1.1 > Cammie 0.6 > Compétence d'arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","vin","cam","ogh"]}
        weapons={["cla","vin","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.1" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=2lpscW2-4aM"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Utilisez la compétence d'arme pour le 2e stun à 27,5 secondes de la fin du combat (càd 32,5 secondes après le début du combat) et 7 secondes (càd 53 secondes après le début du combat) pour le 3e stun. Les valeurs du 3ème stun peuvent s'ajuster bien sûr en fonction de si vous êtes en carry over ou non pour maximiser vos dégâts.</li>
        <li>Si le boss commence à lancer le dash avec ces chaînes avant d'être étourdi la première fois, utilisez la chaîne d'Oghma vers 1.3 pendant la première chaîne.</li>
        "
        chain1="Claude 1.7 > Oghma 0.9 > Cammie 2.4 > Compétence d'arme > Vinette 1.1"           
        chain2="Claude 1.5 > Oghma 0.9 > Cammie 2.4 > Compétence d'arme > Vinette 1.1"
        chain3="Claude 0.3 > Compétence d'arme  "
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","nar","cam","and"]}
        weapons={["cla","dark","cam","dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.2" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=1QrT14REoKI"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Attendez que Nari et Andras utilisent les derniers coups de leur chaîne d'attaque avant d'utiliser leurs chaînes.</li>
        <li>Utilisez la chaîne de Claude dans un timing tel qu'Andras atteindra le 3ème + 4ème coup de son attaque normale lorsque le boss est sous l'état aéroporté. Dans la vidéo, la 2ème chaîne de Claude a été utilisée très tôt pour y parvenir.</li>
        <li>Bien que la compétence d'arme de la première chaîne ait été utilisée tôt, de sorte que la totalité de l'affection ne soit pas appliquée, je ne pense pas que cela soit vraiment important, car vous serez toujours en mesure d'obtenir deux chaînes complètes.</li>
        "
        chain1="Claude 1.5 > Andras 0.7 > Nari 1.3 > Compétence d'Arme"           
        chain2="Claude 2.1 > Andras 1.0 > Nari 1.2 > Compétence d'Arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

        <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","nar","cam","ogh"]}
        weapons={["cla","dark","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=Nilj4NbsAI8"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Attendez que Nari fasse le dernier coup de sa chaîne d'attaque avant d'utiliser la compétence en chaîne la première fois. Dans la vidéo, elle l'a fait juste avant que le boss ne soit étourdi, ce qui fait que la chaîne a été utilisée immédiatement.</li>"
        chain1="Claude 1.2 > Oghma 0.5 > Cammie 2.4 > Compétence d'Arme"           
        chain2="Nari 2.4"
        chain3="Claude 1.3 > Oghma 1.2 > Cammie 2.4 > Compétence d'Arme"
        chain4="Nari 2.4"
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 


        <Team onVideoPlay={onVideoPlay} 
        heroes={["cla","kam","cam","ogh"]}
        weapons={["cla","dark","cam","ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=9moUPeUjEfk"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Claude 1.2 > Oghma 0.5 > Cammie 2.4 > Compétence d'Arme > Kamael 1.1"           
        chain2="Claude 1.2 > Oghma 0.5 > Cammie 2.4 > Compétence d'Arme > Kamael 1.1"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}