import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>

<Team onVideoPlay={onVideoPlay} 
heroes={["kai","val","sha","lap"]}
weapons={["kai","val","sha","lap"]}
access={["mino", "sg", "sg", "sg"]}
dmg="" cards={["skill-skill","atk7-atk5","crit-crit","crit-light"]}    
video="/static/videos/s56-duncan-dark.mp4"
videoSpaceship=""
relic="cup"
pattern="1"
warning="
<li>Positionnez vous de telle façon à ce que les attaques du boss n'interrompt pas vos alliés d'attaquer.</li> 
"
chain1="Valencia (1.5) > Lapice (2.4) > Kai (1.5) > Shapira (2.4) > CA"           
chain2="Valencia (1.5) > Lapice (2.4) > Kai (1.5) > Shapira (2.4) > CA"
chain3=""
chain4=""
chain5=""
chainSpaceship1=""
chainSpaceship2=""
chainSpaceship3=""
chainSpaceship4=""
chainSpaceship5=""
/> 

<Team onVideoPlay={onVideoPlay} 
heroes={["chu","cam","gab","and"]}
weapons={["chu","cam","gab","light"]}
access={["mino", "sg", "sg", "sg"]}
dmg="" cards={["skill-skill","atk5-dark","atk5-atk5","atk7-light"]}    
video="https://www.youtube.com/watch?v=cMaLx11W8RQ"
videoSpaceship=""
relic="book"
pattern="1"
warning="  
<li>Les compétence en chaîne d'Andras et de Chun sont utilisés après le dernier coup de leur séquence d'attaque normale.</li>
<li>Positionnez vous de telle façon à ce que les attaques du boss n'interrompt pas vos alliés d'attaquer.</li> 
"
chain1="Chun (1.5) > Gabrielle (1.4) > Cammie (2.4) > CA > Andras (0.7)"           
chain2="Chun (1.3) > Gabrielle (1.3) > Cammie (2.4) > CA > Andras (0.7)"
chain3="Chun (0.9)"
chain4=""
chain5=""
chainSpaceship1=""
chainSpaceship2=""
chainSpaceship3=""
chainSpaceship4=""
chainSpaceship5=""
/>    

<Team onVideoPlay={onVideoPlay} 
heroes={["gab","chu","cam","and"]}
weapons={["gab","chu","cam","light"]}
access={["mino", "sg", "sg", "sg"]}
dmg="" cards={["skill-skill","atk5-atk5","atk5-dark","atk7-light"]}    
video="https://youtu.be/xBl3zzSUqfY"
videoSpaceship=""
relic="book"
pattern="1"
warning="           
<li>Positionnez vous de telle façon à ce que les attaques du boss n'interrompt pas vos alliés d'attaquer.</li> 
"
chain1="Chun (1.3) > Gabrielle (1.4) > Cammie (2.0) > CA > Andras (1.6)"           
chain2="Chun (1.3) > Gabrielle (1.4) > Cammie (2.0) > CA > Andras (1.1)"
chain3=""
chain4=""
chain5=""
chainSpaceship1=""
chainSpaceship2=""
chainSpaceship3=""
chainSpaceship4=""
chainSpaceship5=""
/>    

<Team onVideoPlay={onVideoPlay} 
heroes={["kai","lil","lap","val"]}
weapons={["kai","lil","lap","val"]}
access={["mino", "sg", "sg", "sg"]}
dmg="" cards={["skill-skill","atk7-dark","crit-crit","crit-light"]}    
video="https://youtu.be/dURyQPSj8TY"
videoSpaceship=""
relic="cup"
pattern="1"
warning=""
chain1="Kai (1.6) > Lapice (1.6) > Valencia (1.5) + CA > Lilith (0.9)"          
chain2="Kai (1.6) > Lapice (0.2) > CA > Valencia (1.5) > Lilith (0.2) + CA"          
chain3=""
chain4=""
chain5=""
chainSpaceship1=""
chainSpaceship2=""
chainSpaceship3=""
chainSpaceship4=""
chainSpaceship5=""
/> 

<Team onVideoPlay={onVideoPlay} 
heroes={["yuz","rey","lil","kai"]}
weapons={["yuz","light","lil","kai"]}
access={["mino", "sg", "sg", "sg"]}
dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
video="https://youtu.be/7xkugQzDcac"
videoSpaceship=""
relic="book"
pattern="1"
warning="           
<li><b>La compétence d'arme qui stun Duncan la 2ème fois, annule la téléportation au milieu. Dans la vidéo, le timing est très serré. Vous pouvez utiliser la compétence en chaîne de Lilith un peu plus tôt</b>, pour vous assurer qu'il a déjà commencé l'animation de téléportation ou qu'il vient juste de terminer la téléportation, au moment où vous le stun.</li> 
"
chain1="Kai (1.2) > Lilith (1.1) > CA > Yuze (0.8)"           
chain2="Kai (1.1) > Lilith (1.1) > CA > Yuze (0.8)"
chain3=""
chain4=""
chain5=""
chainSpaceship1=""
chainSpaceship2=""
chainSpaceship3=""
chainSpaceship4=""
chainSpaceship5=""
/> 
</>
  );
}