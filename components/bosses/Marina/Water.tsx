import React from "react";
import Team from "../../Team";

export default function Water() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["ame", "may", "kam", "euh"]}
        weapons={["ame", "may", "kam", "earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="43.5" cards={["skill-skill", "crit-crit", "crit-earth", "atk7-atk5"]}
        video="https://www.youtube.com/watch?v=SQalF5tFgOU"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<ul>
        <li>Vous pouvez faire dasher Marina sur place lorsqu'elle fait le triple dash au début, en vous plaçant au bon endroit.</li>
        <li>Au début et après le triple dash/ancre au mur, tenez-vous plus loin que vos alliés, de façon à ce que Marina accroche Ameris.</li>
        <li>Essaie de garder Marina près de Kamael  à l'horizontale ou à la verticale, afin qu'il ne rate pas les derniers coups de sa séquence d'attaque normale.</li>
        <li><b>Vous pouvez également essayer les chaînes suivantes, les dégâts maximums devraient être légèrement plus élevés, mais elles sont plus difficiles à jouer : <a target='_blank' href='https://www.youtube.com/watch?v=NQ7m989e8EM'> Voir la vidéo</a></b>.</li>
        </ul>"
        chain1="Kamael 1.1"
        chain2="Ameris 1.4 > Eunha 0.1 > Compétence d'arme > Kamael 0.5"
        chain3="Ameris 1.4 > Eunha 0.1 > Compétence d'arme > Kamael 1.1"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["ame", "may", "and", "kam"]}
        weapons={["ame", "may", "earth", "kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-crit", "crit-earth", "atk7-atk5"]}
        video="https://youtu.be/nPKb4ADAsNc"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<ul>
        <li><b>Essayez d'utiliser la chaîne d'Andras après son 4ème coup de sa séquence d'attaques normales, si c'est possible.</b></li>
        <li>Vous pouvez faire dasher Marina sur place lorsqu'elle fait le triple dash au début, en vous plaçant au bon endroit.</li>
        <li>Si Marina ne commence pas à marcher avant d'être stun la première fois, utilisez la chaîne de Mayreel à <0.8. Cela se produit lorsqu'elle lance son ancre et s'éloigne assez loin, car le reste des attaques est alors retardé.</li>
        <li>Mettez de préférence des Skill DMG en stats sur la relique d'Euhna.</li>
        </ul>"
        chain1="Ameris (1.6) > Mayreel (1.1)"
        chain2="Kamael (1.1) > Andras (0.9)"
        chain3="Ameris (1.3) > Mayreel (1.3)"
        chain4="Andras (2) + Kamael (1.7)"
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["ame", "tin", "may", "kam"]}
        weapons={["ame", "tin", "may", "kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-crit", "crit-earth"]}
        video="https://youtu.be/6sEaK65NUsM"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<ul>
        <li>Vous pouvez faire dasher Marina sur place lorsqu'elle fait le triple dash au début, en vous plaçant au bon endroit.</li>
        <li>Si Marina ne commence pas à marcher avant d'être stun la première fois, utilisez la chaîne de Mayreel à <0.8. Cela se produit lorsqu'elle lance son ancre et s'éloigne assez loin, car le reste des attaques est alors retardé.</li>
        <li>Mettez de préférence des Skill DMG en stats sur la relique d'Euhna.</li>
        </ul>"
        chain1="Ameris (1.4) > Mayreel (1.6)"
        chain2="Kamael (1.1) > Tinia (0.2) > CA > Ameris (0.9) > Mayreel (0.6)"
        chain3="Kamael (1.1) > Tinia (0.8) > CA > Ameris (0.8)"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["rue", "rey", "yuz", "lil"]}
        weapons={["rue", "earth", "earth", "lil"]}
        access={["mirror", "sg", "sg", "sg"]}
        dmg="37.2" cards={["skill-skill", "crit-earth", "crit-crit", "atk7-atk5"]}
        video="https://www.youtube.com/watch?v=Oz2ezc0mOsM"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li><b>Mettez un accessoire Miroir Terre si il a de meilleures statistiques que votre collier Minotaure. Rue n'a pas besoin de Crit et préfère des stats autour de sa compétence d'arme.</b></li>
        <li>Le fait d'ammener Marina vers le mur, vous permet de la faire rester sur place pendant ses dashs.</li>
        </ul>"
        chain1="Lilith (1.2) > Rue (1.2)"
        chain2="Rey (0.1) > Yuze (1.1)"
        chain3="Lilith (1.4) > Rue (1.2)"
        chain4="Rey (0.1) > Yuze (1.1)"
        chain5="Lilith (1.1) > Rue (1.2)"
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["tin", "kam", "may", "and"]}
        weapons={["aob", "kam", "may", "earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk5-earth", "crit-atk7", "crit-crit"]}
        video="https://www.youtube.com/watch?v=EThWfjoGl8U"
        videoSpaceship=""
        relic="book"  
        pattern="1"
        warning="<ul>
        <li><b>Entre la variante avec l'arme d'Aoba ou l'arme de Tinia, choisissez celui l'arme sur laquelle vous avez le plus de rupture de limite ou avec lequel vous vous sentez le plus à l'aise.</b></li>
        <li><b>Essayez d'utiliser la chaîne d'Andras après son 4ème coup de sa séquence d'attaques normales, si c'est possible.</b></li>
        <li>Veillez à synchroniser la marque de Tinia pendant les chaînes pour maximiser vos dégâts.</li>
        </ul>"
        chain1="Andras (1.8) > Mayreel (1.1) > CA"
        chain2="Kamael (0.7) > Marque > Tinia (0.6) > CA > Andras (0.6) > Mayreel (0.9) > CA"
        chain3="Kamael (0.7) > Marque > Tinia (0.6) > CA > Andras (0.6)"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["tin", "kam", "may", "and"]}
        weapons={["tin", "kam", "may", "earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk5-earth", "crit-atk7", "crit-crit"]}
        video="https://www.youtube.com/watch?v=DQNs1nrJCwA"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li><b>Entre la variante avec l'arme d'Aoba ou l'arme de Tinia, choisissez celui l'arme sur laquelle vous avez le plus de rupture de limite ou avec lequel vous vous sentez le plus à l'aise.</b></li>
        <li><b>Essayez d'utiliser la chaîne d'Andras après son 4ème coup de sa séquence d'attaques normales, si c'est possible.</b>
        <li>Veillez à synchroniser la marque de Tinia pendant les chaînes pour maximiser vos dégâts.</li>
        </ul>"
        chain1="Andras (0.1) > Mayreel (0.6) > Marque > Tinia (0.9) > CA > Kamael (0.5)"
        chain2="Andras (1.6) > Marque > Mayreel (0.3) > Tinia (2.4) > CA > Marque > Kamael (0.5)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
