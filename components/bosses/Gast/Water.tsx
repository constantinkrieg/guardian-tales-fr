import React from "react";
import Team from "../../Team";

export default function Tams({ onVideoPlay }) {
    return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "kam", "tin", "ame"]}
        weapons={["bar2", "kam", "tin", "ame"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="47.7" cards={["skill-skill", "atk5-earth", "crit-atk7", "crit-crit"]}
        video="https://www.youtube.com/watch?v=M-G0joWgAwQ"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["ame", "kam", "cam", "and"]}
        weapons={["ame", "kam", "cam", "earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="47.6" cards={["skill-skill", "crit-earth", "atk7-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=qXoySqvEir0"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="  
            <li>Après le coup initial, Gast marchera soit dans le sens des aiguilles d'une montre, soit dans le sens inverse : vous pouvez voir comment vous pouvez facilement coincer Gast contre le mur en fonction de ce qui se passe grâce <a href='https://www.youtube.com/watch?v=AAXhLwrQx1w' target='_blank'>à cette vidéo</a>. Le fait de le coincer contre le mur entraîne généralement des dégâts plus importants.         
            <li><b>Essayez de faire en sorte que les attaques de Gast ne touchent pas vos alliés, sans trop vous éloigner d'eux.</b></li> 
            <li>Utiliser la Chaîne d'Andras après le 4ème coup.</li>
            <li>Placez le laser rotatif loin de vos alliés, afin qu'ils ne soient pas autant interrompus.</li>
            <li>Pour la relique d'Ameris, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
            "
        chain1="Kamael 1.1 > Cammie 1.4 > Ameris 1.4 > Compétence d'arme > Andras 0.5"
        chain2="Kamael 1.1 > Cammie 0.4 > Compétence d'arme > Ameris 1.4 > Andras 0.2"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    

      <Team onVideoPlay={onVideoPlay}
        heroes={["rue", "bar", "yuz", "lil"]}
        weapons={["rue", "bar", "earth", "lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45" cards={["skill-skill", "crit-crit", "crit-earth", "atk7-dark"]}
        video=""
        videoSpaceship="https://www.youtube.com/watch?v=K2vHsew4k0c"
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />   

      <Team onVideoPlay={onVideoPlay}
        heroes={["rue", "bar", "yuz", "lil"]}
        weapons={["rue", "bar", "earth", "lil"]}
        access={["mirror", "sg", "sg", "sg"]}
        dmg="44.5" cards={["skill-skill", "crit-crit", "crit-earth", "atk7-dark"]}
        video=""
        videoSpaceship="https://www.youtube.com/watch?v=kSelcc2mIek"
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />    

      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "cam", "ame", "kam"]}
        weapons={["bar2", "cam", "ame", "kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-dark", "crit-crit", "crit-earth"]}
        video="https://youtu.be/o7mw1GjzDko"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1="Cammie 0.1 > Ameris 1.4 + Compétence d'arme > Eunha 1.0 > Kamael 1.2 + Compétence d'arme"
        chain2="Cammie 0.1 > Ameris 1.4 + Compétence d'arme > Eunha 1.0 > Kamael 1.2 + Compétence d'arme"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["ame", "may", "kam", "euh"]}
        weapons={["ame", "may", "kam", "earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="44.3" cards={["skill-skill", "crit-earth", "crit-atk7", "crit-atk5"]}
        video="https://www.youtube.com/watch?v=3ayCHtDerck"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["ame", "may", "tin", "kam"]}
        weapons={["ame", "may", "tin", "kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="42" cards={["skill-skill", "crit-earth", "crit-atk7", "crit-atk5"]}
        video="https://www.youtube.com/watch?v=FSu9Fy5MxcY"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["rue", "lil", "yuz", "rey"]}
        weapons={["rue", "lil", "earth", "earth"]}
        access={["mirror", "sg", "sg", "sg"]}
        dmg="39" cards={["skill-skill", "crit-dark", "crit-crit", "atk7-earth"]}
        video="https://www.youtube.com/watch?v=VVeEbA2JDJo"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Rey (1.6) > Rue (1.1) > CA > Yuze (1.1)"
        chain2="Lilith (0.7)"
        chain3="Rey (ASAP) > Rue (ASAP) > CA > Yuze (ASAP)"
        chain4="Lilith (0.3)"
        chain5="Rey (ASAP) > Rue (ASAP) > CA > Yuze (ASAP)"
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      {/*<Team onVideoPlay={onVideoPlay}
        heroes={["tin", "kam", "may", "and"]}
        weapons={["tin", "kam", "may", "earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-crit", "crit-earth"]}
        video="https://youtu.be/Up6Y3OyNrmk"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
            <li><b>Essayez de faire en sorte que les attaques de Gast ne touchent pas vos alliés, sans trop vous éloigner d'eux.</b></li> 
            <li>Placez le laser rotatif loin de vos alliés, afin qu'ils ne soient pas autant interrompus. 
            "
        chain1="Andras (1.2) > Mayreel (2) > Tinia (1.9) > CA > Kamael (0.7)"
        chain2="Andras (1) > Mayreel (2) > Tinia (Direct) > CA > Kamael (0.9)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />*/}
    </>
  );
}
