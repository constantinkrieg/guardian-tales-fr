import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>

<Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","gab","tin"]}
        weapons={["and","chu","gab","light"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.1" cards={["crit-crit","crit-atk7","crit-light","atk5-atk5"]}   
        video="https://www.youtube.com/watch?v=1ukzngK18so"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Pensez à détruire les tourelles en priorité au début du combat.</li>
        "        
        chain1="Andras 0.5 > Chun 1.5 > CA > Gabrielle 0.6 > Tinia 0.7"
        chain2="Andras 1.5 > Chun 1.2 > CA > Gabrielle 1.0 > Tinia 1.0"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
      
      <Team onVideoPlay={onVideoPlay} 
        heroes={["ogh","vin","cam","cla"]}
        weapons={["ogh","vin","cam","cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=RnA6Rwdz6M8"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Pensez à détruire les tourelles en priorité au début du combat.</li>
        "
        chain1="Oghma 0.6 > Cammie 2.0 > CA > Vinette 1.1 > Claude 0.5 > CA"
        chain2="Oghma 1.3 > Cammie 2.0 > CA > Vinette 1.1 > Claude 1.3 > CA"
        chain3="Oghma 2.5"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["ame","may","kam","euh"]}
        weapons={["ame","may","kam","earth"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-atk7","crit-earth","crit-atk5"]}    
        video="https://www.youtube.com/watch?v=xzVyHrzghYg"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Pensez à détruire les tourelles en priorité au début du combat.</li>
        "        
        chain1="Ameris 1.6 > Mayreel 0.9 > CA"
        chain2="Kamael 1.1 "
        chain3="Ameris 1.5 > Mayreel 0.9 > CA"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
