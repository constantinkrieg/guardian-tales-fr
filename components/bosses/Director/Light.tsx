import React from "react";
import Team from "../../Team";
import { Callout } from 'nextra-theme-docs'

export default function Light() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "lah", "kan", "nar"]}
        weapons={["euh", "lah", "kan", "nar"]}
        access={["euh", "lah", "kan", "nar"]}
        dmg="" cards={["skill-skill", "crit-crit", "atk5-atk5", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=GrBy5318ijY"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        <li><b>Utilisez votre compétence d'arme 39-39.5 après le début du combat pour débuter votre 2ème chaîne.</b></li>
        "
        chain1="Kanna 1.0 > Nari 0.7 > Lahn 1.5 > Eunha 2.4 > CA"
        chain2="Kanna 1.0 > Nari 0.7 > Lahn 1.5 > Eunha 2.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />


    <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "kni"]}
        weapons={["euh", "nar", "kan", "kni"]}
        access={["euh", "lah", "kan", "nar"]}
        dmg="" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video=""
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>N'utilisez pas l'attaque secondaire de Euhna - sa marque pose conflit avec celle de Kanna et enlève la réduction de défense au boss.</b></li>
        <li>Pour la relique d'Euhna, utilisez de préférence une avec de bonnes stats en Dégâts de compétence.</li>
        "
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["fk", "and", "nar", "kan"]}
        weapons={["fk", "ido", "nar", "kan"]}
        access={["euh", "lah", "kan", "nar"]}
        dmg="" cards={["crit-skill", "crit-crit", "atk7-atk5", "atk5-atk5"]}
        video="https://www.youtube.com/watch?v=EzscPUV97TM"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<li>Faites en sorte de ne pas être à court de munitions avec FK, ne maintenez pas le bouton vers la fin</li>"
        chain1="Nari 0.8 > Kanna 0.8 > FK 0.8 > Andras 1.1"
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />    

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "kan", "nar", "kni"]}
        weapons={["ido", "kan", "nar", "kni"]}
        access={["euh", "lah", "kan", "nar"]}
        dmg="" cards={["crit-crit", "atk5-atk5", "crit-atk7", "atk5-atk5"]}
        video=""
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>La run côté survie est vraiment compliquée. Cette équipe n'en vaut pas vraiment la peine. Si vous mourez, vous ferez bien moins que la team évoqué juste en dessous alors on vous recommande pas d'utiliser cette équipe sauf pour les plus fous d'entre vous qui n'ont pas Euhna.</li>"
        chain1="Kanna 0.3 > Nari 0.7 > Knight 0.1 > Andras 0.9"
        chain2="Kanna 0.3 > Nari 0.9 > Andras 0.9 > Knight ASAP"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />              
    </>
  );
}
