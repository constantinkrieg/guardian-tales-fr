import React from "react";
import Team from "../../Team";
import { Callout } from 'nextra-theme-docs'

export default function Light() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["kai", "val", "lap", "sha"]}
        weapons={["kai", "val", "lap", "sha"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-light", "crit-crit"]}
        video="https://www.youtube.com/watch?v=UYs5gXJ009Y"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Jouer Valencia en lead est tout aussi fort, vous pouvez donc la jouer si vous préférez (le timing des chaines sera différent).</li>
        <li>Faites attention lorsque vous utilisez la CA (CA) quand le boss est loin d'un coin. Vous ne pourrez probablement pas le toucher complètement et donc ne pourrez donc pas l'étourdir avec 2 CA.</li>
        "
        chain1="Valencia 1.4 > Shapira 0.1 > CA > Kai 1.5 > 0.9-1.0 Lapice > CA"
        chain2="Valencia 1.4 > Shapira 2.4 > Kai 1.5 > 2.4 Lapice > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["val", "lap", "kai", "sha"]}
        weapons={["val", "lap", "kai", "sha"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk5-light", "crit-crit", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=XINl_d3CRt4"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Jouer Kai en lead est tout aussi fort, vous pouvez donc le jouer si vous préférez (le timing des chaines sera différent).</li>
        <li>Faites attention lorsque vous utilisez la CA (CA) quand le boss est loin d'un coin. Vous ne pourrez probablement pas le toucher complètement et donc ne pourrez donc pas l'étourdir avec 2 CA.</li>
        "
        chain1="Valencia 1.4 > Lapice 1.6 > CA > Kai 1.5 > Shapira 2.5"
        chain2="Valencia 1.4 > Lapice 1.6 > CA > Kai 1.5 > Shapira 0.5 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "cam", "gab"]}
        weapons={["and", "chu", "cam", "gab"]}
        access={["mino", "minotaur", "minotaur", "sg"]}
        dmg="" cards={["skill-skill", "atk5-atk5", "atk5-dark", "atk7-light"]}
        video="https://www.youtube.com/watch?v=4IPXKT3r4qg"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Vous pouvez également essayer de laisser les hologrammes apparaître et de donner à tout le monde des cartes 'Attaque quand ennemi tué' en suivant <a target='_blank 'href='https://www.youtube.com/watch?v=iD7WUZ68KNo'>cette méthode</a>.</li>
        <li>Les déplacements du boss font un triangle, donc en faisant attention aux trois premiers coins où il se rend et dans quel ordre, vous saurez toujours où il ira ensuite. Cela vaut également pour l'endroit où il se déplace après s'être téléporté au milieu.</li>"
        chain1="Andras 1.5 > Chun 1.5 > CA > Gabriel 1.2 > Cammie 2.4"
        chain2="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.2 > Cammie 1.2"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "gab", "ros"]}
        weapons={["eva", "chu", "gab", "light"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["atkkill-atkkill", "atkkill-atkkill", "atkkill-atkkill", "atkkill-atkkill"]}
        video="https://www.youtube.com/watch?v=6wjo3ramqgI"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Laisser les hologrammes apparaître et utiliser les cartes 'Attaque quand ennemi tué' sur tout le monde est probablement mieux, je vous recommande d'essayer.</li>
        "
        chain1="Gabriel 1.4 > Rosetta 1.8 > Andras 1.4 > Chun 2.0"
        chain2="Gabriel 1.5 > Rosetta 1.7 > Andras 1.4 > Chun 1.7"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  
      
      <Team onVideoPlay={onVideoPlay}
        heroes={["kai", "lap", "val", "lil"]}
        weapons={["kai", "lap", "val", "lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-light", "crit-crit", "atk7-dark"]}
        video="https://youtu.be/g5-9v0Yt_bw?si=IbXHxhWwjx_9_mr3"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Faites attention au moment où vous utilisez votre CA en dehors des chaînes, afin d'appliquer l'intégralité de l'affliction.</li>
        "
        chain1="Valencia 1.0 > Lapice 0.8 > CA > Kai 0.9 > Lilith 0.5 > CA"
        chain2="Valencia 1.4 > Lapice 0.9 > CA > Kai 1.7 > Lilith 2.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />           
    </>
  );
}
