import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay} 
        heroes={["ogh","vin","cam","cla"]}
        weapons={["ogh","vin","cam","cla"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","atk7-atk5","crit-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=oljKsnZsTzc"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Oghma 1.1 > Cammie 1.4 > CA > Vinette 1.1 > Claude 1.6 > CA"
        chain2="Oghma 1.7 > Cammie 1.6 > CA > Vinette 0.7 > Claude 1.6 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
