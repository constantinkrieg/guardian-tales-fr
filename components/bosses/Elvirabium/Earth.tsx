import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","pli","cam","kam"]}
        weapons={["dol","pli","cam","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","crit-fire","atk7-dark","atk5-earth"]}    
        video="https://www.youtube.com/watch?v=YnHuVc1u74g"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","pli","cam","kam"]}
        weapons={["dol","pli","cam","kam"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","crit-fire","atk7-dark","atk5-earth"]}    
        video="https://www.youtube.com/watch?v=YnHuVc1u74g"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
      
      <Team onVideoPlay={onVideoPlay} 
        heroes={["rey","sci","lil","yuz"]}
        weapons={["rey","sci","lil","fire"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-skill","crit-fire","crit-shield","atk7-shield"]}    
        video="https://www.youtube.com/watch?v=iU50C3yGtm0"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
