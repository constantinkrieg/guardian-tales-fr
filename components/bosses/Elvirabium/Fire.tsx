import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","kam","cam","orc"]}
        weapons={["and","kam","cam","orc"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-skill","atk5-earth","crit-dark","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=4exheciChF8"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["yuz","whi","lil","rey"]}
        weapons={["yuz","whi","lil","water"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-atk7","crit-dark","crit-atk5"]}    
        video="https://www.youtube.com/watch?v=tpTZiF1JDgE"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["euh","nar","kan","lah"]}
        weapons={["euh","nar","kan","lah"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","atk5-atk5","crit-atk7"]}    
        video="https://www.youtube.com/watch?v=WoI1OwPKxtI"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""           
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 
    </>
  );
}
