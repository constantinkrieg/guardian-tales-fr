import React from "react";
import Team from "../../Team";
 
export default function Earth() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","cam","pli"]}
        weapons={["dol","chu","cam","pli"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.5" cards={["skill-crit","atk7-light","atk5-dark","crit-crit"]}    
        video="https://www.youtube.com/watch?v=DBxOwefqSDE"
        videoSpaceship=""
        relic="cup" 
        pattern="1"
        warning="
        <li>Le timing de la chaîne de Plitvice varie en fonction du nombre d'attaques que vous devez annuler. Si le panda ne bouge pas pendant que la chaîne de Chun est lancé, utilisez la chaîne de Plitvice vers 1.0-1.1, sinon 1.4-1.5.</li>
        <li>Le mouvement du boss au départ peut être contrôlé dans une certaine mesure, il est donc possible de réduire la probabilité que les alliés se retrouve coincée derrière les flammes. L'ordre des personnages dans l'équipe est probablement déterminant à cet égard.</li>
        "
        chain1="Chun 0.3 > Andras 1.4 > Plitvice 1.5 + CA"
        chain2="Chun 0.5 > Andras 1.4 > Plitvice 0.7 + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />


      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","pli","cam","nar"]}
        weapons={["dol","pli","cam","nar"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="46.1" cards={["skill-skill","crit-crit","crit-dark","atk7-atk5"]}    
        video="https://www.youtube.com/watch?v=OH8MqTn0NWA"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Le timing de la chaîne de Plitvice varie en fonction du nombre d'attaques que vous devez annuler. Si le panda ne bouge pas pendant que la chaîne de Chun est lancé, utilisez la chaîne de Plitvice vers 1.0-1.1, sinon 1.4-1.5.</li>
        <li>Vous pouvez utiliser des cartes/reliques Crit ou Dégâts de Compétence. On conseille Dégâts de Compétence si vous utilisez des Coupes, et Crit si vous utilisez des Livres ou un mélange des deux.</li>
        <li>Le mouvement du boss au départ peut être contrôlé dans une certaine mesure, il est donc possible de réduire la probabilité que les alliés se retrouve coincée derrière les flammes. L'ordre des personnages dans l'équipe est probablement déterminant à cet égard.</li>
        "
        chain1="Nari 0.5 > Andras 1.4 > Plitvice 1.0 + CA"
        chain2="Nari 0.5 > Andras 1.4 > Plitvice 1.0 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />



      <Team onVideoPlay={onVideoPlay} 
        heroes={["rey","sci","win","lil"]}
        weapons={["rey","sci","win","lil"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45.8" cards={["skill-skill","crit-crit","crit-fire","atk7-dark"]}    
        video="https://youtu.be/utauxtUqa7Q"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Éloignez-vous du boss lorsque vous utilisez la chaîne de Lilith, afin de déclencher l'attaque des fusée. Si vous ne le faites pas, vous obtiendrez le jump pattern, qui peut annuler la chaîne de Lilith si vous l'utilisez trop tard. Si vous n'y arrivez pas, restez près du Panda et utilisez la chaîne de Lilith à 0.2-0.3 pour être sûr.
        <li>Après le contre au début, utilisez-le toujours lorsque vous êtes sur le point d'être frappé par le swing de la boule et utilisez la CA juste après.</li>
        <li>Le mouvement du boss au départ peut être contrôlé dans une certaine mesure, il est donc possible de réduire la probabilité que les alliés se retrouve coincée derrière les flammes. L'ordre des personnages dans l'équipe est probablement déterminant à cet égard.</li>
        "
        chain1="Lilith 0.1 > Scintilla 1.0-1.1 + CA > Rey 0.1 > Winling 1.0-1.1 + CA"
        chain2="Lilith 0.1 > Scintilla 1.0-1.1 + CA > Rey 0.1 > Winling 1.0-1.1 + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        /> 

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","luc","vis"]}
        weapons={["dol","chu","luc","vis"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="45.5" cards={["skill-skill","atk7-light","crit-fire","crit-crit"]}    
        video="https://www.youtube.com/watch?v=LE2DDBeCBJ8"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Assurez-vous de jouer avec la compétence alternative de Vishuvac. Le fait qu'elle soit active lorsque la compétence d'arme frappe pendant les chaînes est très important pour les dégâts globaux. Ajustez le timing des chaînes en conséquence. Si elle utilise la compétence juste avant d'utiliser la chaîne d'Andras, cela s'alignera parfaitement.</li>
        <li>Le timing des chaînes dépend du combat. En gros, on veut que le boss fasse les swings après chaque chaînes. Comme vous pouvez le voir dans la vidéo, il ne l'a pas fait à la fin, ce qui est dû à un mauvais timing. Selon le moment exact où vous étourdissez le boss, vous devez annuler 1, 2 ou 3 de ses mouvements. Le dernier coup à annuler est celui où le panda commence à marcher, comme vous pouvez le voir dans la vidéo . Les timing des chaînes pour annuler les coups sont les suivants : Andras <1.4, Nari <0.9, Lucy <1.0.</li>
        <li>On recommande également de regarder <a href='https://www.youtube.com/watch?v=su8PB4yu_oM' target='_blank'>cette vidéo</a>. L'uploader est très bon pour jouer cette équipe et vous pouvez le voir varier les temps de chaîne en fonction de la situation dans les 5 runs qu'il fait.</li>"
        chain1="Andras 1.0 > Chun 1.5 > Lucy 1.1 + Vish 1.0 + CA"
        chain2="Andras 0.9 > Nari 0.8 > Lucy 1.6 + Vish 1.5 + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","vis","luc","nar"]}
        weapons={["dol","vis","luc","nar"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","crit-fire","atk7-atk5","atk5-atk5"]}    
        video="https://www.youtube.com/watch?v=LE2DDBeCBJ8"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Assurez-vous de jouer avec la compétence alternative de Vishuvac. Le fait qu'elle soit active lorsque la compétence d'arme frappe pendant les chaînes est très important pour les dégâts globaux. Ajustez le timing des chaînes en conséquence. Si elle utilise la compétence juste avant d'utiliser la chaîne d'Andras, cela s'alignera parfaitement.</li>
        <li>Le timing des chaînes dépend du combat. En gros, on veut que le boss fasse les swings après chaque chaînes. Comme vous pouvez le voir dans la vidéo, il ne l'a pas fait à la fin, ce qui est dû à un mauvais timing. Selon le moment exact où vous étourdissez le boss, vous devez annuler 1, 2 ou 3 de ses mouvements. Le dernier coup à annuler est celui où le panda commence à marcher, comme vous pouvez le voir dans la vidéo . Les timing des chaînes pour annuler les coups sont les suivants : Andras <1.4, Nari <0.9, Lucy <1.0.</li>
        <li>On recommande également de regarder <a href='https://www.youtube.com/watch?v=su8PB4yu_oM' target='_blank'>cette vidéo</a>. L'uploader est très bon pour jouer cette équipe et vous pouvez le voir varier les temps de chaîne en fonction de la situation dans les 5 runs qu'il fait.</li>"
        chain1="Andras 0.4 > Nari 0.8 > Lucy 1.1 + Vish 1.0 + CA"
        chain2="Andras 0.7 > Nari 0.6 > Lucy 1.1 + Vish 1.0 + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />

        <Team onVideoPlay={onVideoPlay} 
        heroes={["elv","euh","luc","vis"]}
        weapons={["elv","fire","luc","vis"]}
        access={["mino", "ss", "sg", "sg"]}
        dmg="" cards={["crit-skill","atk5-atk5","crit-atk7","crit-fire"]}    
        video="https://www.youtube.com/watch?v=z78LBTunwwY"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li><b>Si le niveau du boss est inférieur à 86, les teams Elvira sont les plus puissantes dû à son passif. 🔻<br/>Après ce seuil plus le niveau du boss est élevé, moins Elvira fera de dégâts, et sera donc dépasser par d'autres équipes.</b></li>
        <li>Pour un positionnement correct des compétences d'armes, essayez de trouver un indice, par exemple le positionnement de la barre HP du boss (dépend de la résolution de l'écran également).</li>
        <li>La chaîne de Vishuvac est utilisé juste après qu'elle ait utilisé sa compétence alternative. Vous pouvez soit retarder légèrement celle de Lucy si le timing n'est pas trop décalé, soit retarder le moment où vous étourdissez le boss pour aider à aligner le timing (vous voulez utiliser la compétence d'arme pour étourdir le boss quand Vishuvac utilise sa compétence alternative).</li>
        <li>Après la 1ère chaîne, faites en sorte que le 2ème swing touche Vishuvac pour retarder sa compétence, car il s'alignera alors sur la 2ème chaîne.</li>
        <li>La chaîne d'Elvira est utilisée très tôt pour bénéficier du bonus de dégâts critiques de la chaîne de Lucy.</li>
        "
        chain1="Lucy 0.7 > Nari 0.8 + Vish 0.1 > CA > Elvira 2.0"
        chain2="Lucy 0.8 > Nari 0.8 + Vish 0.3 > CA > Elvira 2.0"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />  

        <Team onVideoPlay={onVideoPlay} 
        heroes={["rey","sci","lil","yuz"]}
        weapons={["rey","sci","lil","fire"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","atk7-dark","crit-fire"]}    
        video="https://www.youtube.com/watch?v=XiIYZRo7siQ"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Si vous trouvez cela trop difficile, reportez-vous à <a href='https://youtu.be/aqmc_poPMyE' target='_blank'>cette vidéo</a>.</li>
        <li>Si vous avez confiance en vos compétences, essayez <a href='https://www.youtube.com/watch?v=b0sPRWKnPrc' target='_blank'>cette version tryhard autrement</a. Comment ça marche, c'est que vous utilisez la chaîne de Lilith très tard pour obtenir un contre pendant la chaîne. Vous vous éloignez avec Rey, pour que le boss tire des missiles au lieu de faire le saut. Cela fonctionne aussi quand on le laisse faire le saut, mais c'est moins fiable</li>
        "
        chain1="Rey 1.0 > Scintilla 1.1 + CA > Lilith 0.5 > CA > Yuze 0.9 "
        chain2="Rey 0.5 > Scintilla 1.1 + CA > Lilith 0.5 > CA > Yuze 1.2 "
        chain3="Rey 0.0"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
        />       
    </>
  );
}