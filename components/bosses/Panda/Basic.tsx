import React from "react";
import Team from "../../Team";

export default function Basic() {
  return (
    <>

    <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "kam", "cam", "and"]}
        weapons={["cla", "kam", "cam", "dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-earth", "crit-dark", "crit-crit"]}
        video=""
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Vidéo non disponible / Chaînes disponible.</li>
        <li>Pensez à vous mettre derrière le boss au début du combat pour faciliter aux mieux les zones de feu envers vos alliés.</li>
        <li><b>Il faut absolument être à la distance maximum avec le boss quand on fait la 3ème compétence d'arme de Claude. Sinon il risque de marcher au lieu de sauter/rocket et ça décale toutes l'ordre de ses attaques de 1.</b></li>
        <li>La chaînes d'Andras est utilisée après le dernier coup de sa séquence d'attaques normales. Andras doit être <1.3</li>
        </ul>"
        chain1=" Claude (1.5) > Kamael (0.9) > Cammie (2.4) > CA"
        chain2="Claude (0.4) > Kamael (1) > Cammie (2.4) > CA > Andras (0.9)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "nar", "cam", "and"]}
        weapons={["cla", "nar", "cam", "dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-dark", "crit-crit"]}
        video=""
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Vidéo et chaînes non disponible </li>
        <li>Pensez à vous mettre derrière le boss au début du combat pour faciliter aux mieux les zones de feu envers vos alliés.</li>
        <li><b>Il faut absolument être à la distance maximum avec le boss quand on fait la 3ème compétence d'arme de Claude. Sinon il risque de marcher au lieu de sauter/rocket et ça décale toutes l'ordre de ses attaques de 1.</b></li>
        </ul>"
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "chu", "cam", "and"]}
        weapons={["cla", "chu", "cam", "dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-light", "crit-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=iJ_T-d_ki9M"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Pensez à vous mettre derrière le boss au début du combat pour faciliter aux mieux les zones de feu envers vos alliés.</li>
        <li><b>Il faut absolument être à la distance maximum avec le boss quand on fait la 3ème compétence d'arme de Claude. Sinon il risque de marcher au lieu de sauter/rocket et ça décale toutes l'ordre de ses attaques de 1.</b></li>
        <li>On ne sait pas trop pourquoi les chaînes Claude et Oghma sont utilisées si tard dans la vidéo au cours de la deuxième chaîne. Les chaînes de Claude et Oghma doivent toutes deux être utilisées <1.1 pour que le pattern du boss reste cohérent.</li>
        <li>Les chaînes d'Andras et de Chun sont utilisées après le dernier coup de leurs séquences d'attaques normales pendant la 2ème chaîne. Andras doit être <1.3 et Chun >1.1</li>
        <li>Gardez vos distances lorsque vous faites la 2ème chaîne de Claude. Si vous êtes trop près, la chaîne risque d'être annulée par le saut de Panda.
        </ul>"
        chain1="Claude (1.3) > Andras (1.4) > Chun (0.9) > CA"
        chain2="Claude (0.5) > Andras (0.5) > Chun (1.8) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "kam", "cam", "ogh"]}
        weapons={["cla", "kam", "cam", "ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-earth", "crit-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=vIkELSSqY0I"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Parfois, le lancer de boulet se dirige vers Claude, alors assurez-vous qu'ils n'annulent pas votre compétence d'arme lorsque cela se produit et gardez votre distance lorsque vous utilisez la 2ème chaîne de Claude. Si vous êtes trop près, la chaîne risque d'être annulée par le saut de Panda.</li>
        <li>Essayez d'utiliser la Chaîne Oghma juste après le dernier coup de sa séquence d'attaques normales, mais elle doit être <1.1.
        </ul>"
        chain1="Claude (1.4) > Oghma (0.3) > Cammie (2.4) > CA > Kamael (1.1)"
        chain2="Claude (0.6) > Oghma (0.6) > Cammie (2.4) > CA > Kamael (1.1)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "nar", "cam", "ogh"]}
        weapons={["cla", "nar", "cam", "ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-dark", "crit-crit"]}
        video="https://youtu.be/QFm5uaARBKA"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Parfois, le lancer de boulet se dirige vers Claude, alors assurez-vous qu'ils n'annulent pas votre compétence d'arme lorsque cela se produit et gardez votre distance lorsque vous utilisez la 2ème chaîne de Claude. Si vous êtes trop près, la chaîne risque d'être annulée par le saut de Panda.</li>
        <li>Essayez d'utiliser la Chaîne Oghma juste après le dernier coup de sa séquence d'attaques normales, mais elle doit être <1.1.
        </ul>"
        chain1="Claude (1.5) > Oghma (0.6) > Cammie (2.4) > CA"
        chain2="Claude (0.7) > Oghma (0.7) > Cammie (2.4) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "chu", "cam", "ogh"]}
        weapons={["cla", "chu", "cam", "ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-light", "crit-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=yqQjmlzRCUY"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="<ul>
        <li>Parfois, le lancer de boulet se dirige vers Claude, alors assurez-vous qu'ils n'annulent pas votre compétence d'arme lorsque cela se produit et gardez votre distance lorsque vous utilisez la 2ème chaîne de Claude. Si vous êtes trop près, la chaîne risque d'être annulée par le saut de Panda.</li>
        <li>Essayez d'utiliser la Chaîne Oghma juste après le dernier coup de sa séquence d'attaques normales, mais elle doit être <1.1.
        </ul>"
        chain1="Claude (1.4) > Oghma (0.6) > Cammie (2.4) > CA"
        chain2="Claude (0.6) > Oghma (0.6) > Cammie (2.4) > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "ogh", "ara", "nar"]}
        weapons={["cla", "ogh", "ara", "dark"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "crit-dark", "atk7-atk5", "atk5-atk5"]}
        video="https://www.youtube.com/watch?v=k07jkRlsfxk"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""        
        chain1="Claude (1.5) > Oghma (0.1) > Arabelle (2.4) > Nari (1.8)"
        chain2="Claude (0.8) > Oghma (0.5) > Arabelle (2.4) > Nari (1.8)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />      

      <Team onVideoPlay={onVideoPlay}
        heroes={["cla", "kam", "tin", "ogh"]}
        weapons={["cla", "kam", "dark", "ogh"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk7-earth", "atk7-atk5", "crit-dark"]}
        video="https://www.youtube.com/watch?v=DKucQ6XcBvU"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""        
        chain1="Claude (1.2) > Oghma (1.3) > Tinia (0.5) > CA > Kamael (1.2)"
        chain2="Claude (1.4) > Oghma (0.4) > Tinia (2.3) > CA > Kamael (0.5)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />          
    </>
  );
}
