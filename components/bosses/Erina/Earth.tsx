import React from "react";
import Team from "../../Team";

export default function Earth() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["rey", "sci", "lil", "win"]}
        weapons={["rey", "sci", "lil", "win"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-crit", "atk7-dark", "crit-fire"]}
        video="https://www.youtube.com/watch?v=Z19sTr7nkIY"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="<li>Attendre un peu avant le 2ème stun</li>
        <li>Si vous avez un bonus de 140+ cases de conquêtes capturé, privilégiez la team avec Yuze qui fera plus de dégâts"
        chain1="Rey 0.4 > CA + Scintilla 1.0 > Lilith 0.4 > CA + Win Ling 1.0"
        chain2="Rey 0.2 > CA + Scintilla 1.0 > Lilith 0.4 > CA + Win Ling 1.0"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "cam", "pli"]}
        weapons={["dol", "nar", "cam", "pli"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-atk5", "atk7-dark", "crit-fire"]}
        video="https://www.youtube.com/watch?v=cmXNHZEfPck"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
            <li><b>Cette team sur ce boss est très compliquée à jouer</b>, si vous avez du mal, nous vous recommandons d'utiliser une team en dessous.</li> 
            <li>Veuillez vous réferez exactement aux mouvements dans la vidéo 
            "
        chain1="Nari (0.7) > Andras (1.4) > Plitvice (1.1) + CA"
        chain2="Nari (0.7) > Andras (1.4) > Plitvice (1.1) + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "nar", "luc", "vis"]}
        weapons={["dol", "nar", "luc", "vis"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-atk5", "atk5-fire", "crit-atk7"]}
        video="https://youtu.be/Gd1hQWhdbaQ"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
            <li><b>Cette team sur ce boss est très compliquée à jouer</b>, si vous avez du mal, nous vous recommandons d'utiliser une team en dessous.</li> 
            <li>Veuillez vous réferez exactement aux mouvements dans la vidéo 
            "
        chain1="Andras (1.1) > Nari (1) > Lucy (1.1) + Vishuvac (0.8) + CA"
        chain2="Andras (1.1) > Nari (1) > Lucy (1.1) + Vishuvac (0.9) + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["and", "chu", "luc", "vis"]}
        weapons={["dol", "chu", "luc", "vis"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit", "atk5-light", "atk5-fire", "crit-atk7"]}
        video="https://www.youtube.com/watch?v=nuiKc1uhsRk"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
            <li><b>Cette team sur ce boss est très compliquée à jouer</b>, si vous avez du mal, nous vous recommandons d'utiliser une team en dessous.</li> 
            <li>Veuillez vous réferez exactement aux mouvements dans la vidéo 
            "
        chain1="Andras (1) > Chun (1) > Lucy (1) + Vish (0.8) + CA"
        chain2="Andras (1.1) > Chun (1) > Lucy (1) + Vish (0.8) + CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "cam", "pli"]}
        weapons={["euh", "nar", "cam", "pli"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk5-atk7", "crit-dark", "crit-crit"]}
        video="https://www.youtube.com/watch?v=yzySOx5F34c"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["luc", "nar", "vis", "and"]}
        weapons={["luc", "nar", "vis", "fire"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "atk7-atk5", "crit-fire", "crit-crit"]}
        video="https://www.youtube.com/watch?v=pgkxNCb7dCs"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
            <li>Penser à bloquer les déplacements du boss avec votre lead afin qu'elle ne bouge pas partout pour faciliter les dégâts de vos alliés.</li>
            "
        chain1="Lucy (1) > Nari (1) + Vishuvac (0.8) > CA > Andras (2)"
        chain2="Lucy (1) > Nari (0.9) + Vishuvac (0.5) > CA > Andras (1.4)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "lah"]}
        weapons={["euh", "nar", "kan", "lah"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video="https://www.youtube.com/watch?v=nEFvfUS2rm0"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
            <li><b>Veuillez vous réferez exactement aux mouvements dans la vidéo.</b>.
            <li>Attendre qu'il reste 31s (lors d'un combat complet donc -29s sur votre temps total d'attaque) avant d'utiliser la compétence d'arme pour entamer la 2ème chaîne et attendre 10sec (lors d'un combat complet donc -50s sur votre temps total d'attaque) avant d'utiliser la compétence pour lancer la 3ème chaine.<li>"
        chain1="Kanna 2.4 > Nari 1.8 > Lahn 1.8 > Eunha 2.4"
        chain2="Kanna 0.8 > Nari 1.8 > Lahn 1.8 > Eunha 2.4 > CA (0.8)"
        chain3="Kanna 0.3 > Nari 1.8 > Lahn 1.8 > Eunha 2.4"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay}
        heroes={["rey", "sci", "lil", "yuz"]}
        weapons={["rey", "sci", "lil", "fire"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill", "crit-crit", "atk7-dark", "crit-fire"]}
        video="https://www.youtube.com/watch?v=pgkxNCb7dCs"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
            <li>En allant à gauche/droite au début, Erina utilise l'attaque de vague après la première chaîne.</li>
            "
        chain1="Rey (2.4) > Scintilla (2.4) > Lilith (2.4) > CA > Yuze (1.3)"
        chain2="Rey (0.9) > CA > Scintilla (1.2) > Lilith (2.4) > Yuze (1.7)"
        chain3="Rey (2.4) > Scintilla (2.1) > Lilith (2.4) > CA > Yuze (1.7)"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
    </>
  );
}
