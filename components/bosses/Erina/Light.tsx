import React from "react";
import Team from "../../Team";

export default function Light() {
  return (
    <>
      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "nar", "kan", "fk"]}
        weapons={["euh", "nar", "kan", "fk"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="59.5" cards={["skill-skill", "crit-atk7", "atk5-atk5", "crit-crit"]}
        video="https://youtu.be/tSw5XMPmdvU"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Par rapport à d'habitude avec cette équipe, pensez à équiper un Collier Minotaure à Euhna</li>
        <li>Utilisez la Compétence d'arme avant la dernière chaîne à environ 10,5-11 secondes de la fin.</li>
        <li>Marchez jusqu'au même point de départ que dans la vidéo pour contrôler le pattern de départ du boss. Si vous avez du mal à le trouver, il est indiqué <a href='https://youtu.be/ncD0t62gSoY?si=JrRiQDszSL_aE_B1&t=11'>ici.</a></li> 
        <li>Bien que vous deviez bloquer Erina, faites attention à ne pas vous faire toucher par elle après la première chaîne d'Eunha, car le fait d'être touché réduira l'apparation de son buff Multiplicateur de Crit.</li>
      "
        chain1="Nari 1.5 > Kanna 0.9 > FK 2.5 > Compétence d'arme"
        chain2="Eunha 1.1"
        chain3="-"
        chain4="Nari 2.5 > Kanna 0.9 > FK 1.8 > Compétence d'arme > Euhna 1.1"
        chain5="Nari 0.3 > Kanna 1.1 > FK 1.8 + Compétence d'arme"
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />  

      <Team onVideoPlay={onVideoPlay}
        heroes={["euh", "kan", "nar", "lah"]}
        weapons={["euh", "kan", "nar", "lah"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="48.9" cards={["skill-skill", "atk5-atk5", "crit-atk7", "crit-crit"]}
        video="https://www.youtube.com/watch?v=2_1sZaYDhS8"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning=""
        chain1=""
        chain2=""
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />                  
    </>
  );
}
