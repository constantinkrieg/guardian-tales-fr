import React from "react";
import Team from "../../Team";
 
export default function Dark() {
  return (
    <>

      <Team onVideoPlay={onVideoPlay} 
        heroes={["sha","lap","val","kai"]}
        weapons={["sha","lap","val","kai"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="46.4" cards={["crit-light","atk5-crit","atk5-atk5","crit-atk7"]}
        video="https://www.youtube.com/watch?v=K1jHWxScldg"
        videoSpaceship=""
        relic="cup"
        pattern="1"
        warning="
        <li>Si vous kitez le boss environ 10 secondes après le début du combat, vous pouvez retarder le prochain coup, ce qui augmentera les dégâts globaux, sinon jouez <u><a href='https://www.youtube.com/watch?v=eAnH2oIGy6Q' target='_blank'>comme ceci</a></u>.</li>
        <li>Si un allié a l'aggro au début du combat, cela dépend du rng de crit, vous pouvez utiliser des cartes de crit et des reliques de crit sur Shapira pour la rendre plus fiable (si vous n'obtenez pas d'aggro au début faites <u><a href='https://www.youtube.com/watch?v=WvfZw8t2grU' target='_blank'>comme ceci</a></u>.</li>
        "
        chain1="Shapira 2.2 > Valencia 1.4 > Lapice 1.6 > CA > Kai 1.4"
        chain2="Shapira 1.3 > Valencia 1.4 > Lapice 1.5 > CA > Kai 1.4"
        chain3="Shapira 1.4"
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","cam","gab"]}
        weapons={["and","chu","cam","gab"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="44.1" cards={["skill-skill","atk5-atk5","atk5-dark","atk7-light"]}    
        video="https://www.youtube.com/watch?v=DuhDi8_ivQ4"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Puisque l'équipe a déjà deux bonus de groupe sur le taux de coups critiques, vous devriez éviter d'ajouter plus de crit, donc il est plus intéressant d'utiliser des Dégâts de compétence sur carte et la relique sur Andras, et de prioriser les cartes et reliques ATK% plutôt que Crit sur les alliés.</li>
        <li>Pour la même raison, il est préférable d'utiliser des colliers Mino Lumière sur Gabrielle et Sombre sur Cammie au lieu des accessoires habituels.</li>
        <li>Utiliser les cristaux d'Andras après le 1er coup permet d'aider à garder l'aggro juste après le 1er tunnel du boss.</li>
        <li>Vous pouvez essayer également ces chaînes si vous préferez cette méthode <u><a href='https://youtu.be/Hn9einTvDsQ' target='_blank'>(Voir la vidéo)</a></u>.
        "
        chain1="Andras 1.4 > Chun 1.4 > CA > Gabrielle 1.0 > Cammie 0.3 > CA"
        chain2="Andras 1.4 > Chun 1.4 > CA > Gabrielle 1.0 > Cammie 0.3 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />


<Team onVideoPlay={onVideoPlay} 
        heroes={["chu","euh","gab","ele"]}
        weapons={["chu","light","gab","ele"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-atk7","crit-crit","atk5-light"]}
        video="https://www.youtube.com/watch?v=M1kQlEbTKVA"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>N'utilisez votre compétence d'arme, chaîne et compétence additionnelle que lorsque Chun recharge son attaque normale, pour une meilleure application du debuff Déf.</li>
        <li>Kiter le boss après la première fois qu'il fait son tunnel pour retarder ses mouvements.
        "
        chain1="Eunha 1.2 > Eleanor 1.0 > Chun 2.1 > CA > Gabriel 1.3"
        chain2="Eunha 1.2 > Eleanor 1.0 > Chun 2.1 > CA > Gabriel 1.3"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","tin","gab"]}
        weapons={["and","chu","light","gab"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-light","atk7-atk5","crit-crit"]}
        video="https://www.youtube.com/watch?v=r0AEQ1wihRY"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Vous pouvez essayer également ces chaînes si vous préferez cette méthode <u><a href='https://www.youtube.com/watch?v=HtdtsFnPulw' target='_blank'>(Voir la vidéo)</a></u>.
        "
        chain1="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.0 > Tinia 0.4 > CA"
        chain2="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.0 > Tinia 0.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","chu","ros","gab"]}
        weapons={["and","chu","light","gab"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="38.8" cards={["skill-skill","atk7-light","atk7-atk5","crit-crit"]}
        video="https://www.youtube.com/watch?v=ZLimR_rKLrI"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.0 > Tinia 0.4 > CA"
        chain2="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.0 > Tinia 0.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["and","gab","tin","nar"]}
        weapons={["and","gab","light","light"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["crit-crit","crit-light","atk5-atk7","atk5-atk5"]}
        video="https://www.youtube.com/watch?v=0PLjJa3_tS8"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning="
        <li>Kitez le boss plus longtemps avant la 1ère chaîne, laisse le boss se jeter sur vous après le 4ème coup de votre attaque normale et utilisez ensuite la compétence d'arme.</li>
        <li>Avant la 2ème chaîne, vous pouvez utilisez la compétence d'arme après le 4ème coup de votre attaque normale, les compétences en châine devraient déjà être disponible.</li>
        "
        chain1="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.0 > Tinia 0.4 > CA"
        chain2="Andras 1.5 > Chun 1.2 > CA > Gabriel 1.0 > Tinia 0.4 > CA"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />

      <Team onVideoPlay={onVideoPlay} 
        heroes={["chu","gab","tin","ele"]}
        weapons={["chu","gab","light","ele"]}
        access={["mino", "sg", "sg", "sg"]}
        dmg="" cards={["skill-skill","crit-crit","crit-light","atk5-atk5"]}
        video="https://www.youtube.com/watch?v=jcxR0X9BnCI"
        videoSpaceship=""
        relic="book"
        pattern="1"
        warning=""
        chain1="Eleanor (1,2) > Chun (1,2) > Gabrielle (1,4) + CA > Tinia (1,2)"
        chain2="Eleanor (1,2) > Chun (1,2) > Gabrielle (1,4) + CA > Tinia (1,4)"
        chain3=""
        chain4=""
        chain5=""
        chainSpaceship1=""
        chainSpaceship2=""
        chainSpaceship3=""
        chainSpaceship4=""
        chainSpaceship5=""
      />
</>
  );
}