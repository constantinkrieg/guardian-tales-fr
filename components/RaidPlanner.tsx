import React from "react";
import { useState } from "react";
import * as Boss from "./bosses";
import ReactPlayer from 'react-player';

export default function RaidPlanner() {
    const [displayElementSelection, setDisplayElementSelection] = useState(false);
    const [displayBossSelection, setDisplayBossSelection] = useState(false);

    const [selectedElement, setSelectedElement] = useState("light");
    const [selectedBoss, setSelectedBoss] = useState("commander");

    const [videoUrl, setVideoUrl] = useState(null);

    const handleVideoChange = (url) => {
        setVideoUrl(url); // Set the URL for the video to be played
        setDisplayVideoOverlay(true); // Show the video overlay
      };

    const bossComponent = selectedBoss.charAt(0).toUpperCase() + selectedBoss.slice(1);
    const elementComponent = selectedElement.charAt(0).toUpperCase() + selectedElement.slice(1);

    const Component = Boss[`${bossComponent}${elementComponent}`];

    const openVideoOverlay = (url) => {
        setVideoToPlay(url);
        setDisplayVideoOverlay(true);
      };
    
      const closeVideoOverlay = () => {
        setVideoToPlay(null);
        setDisplayVideoOverlay(false);
      };

    const [displayVideoOverlay, setDisplayVideoOverlay] = useState(false);
    const [videoToPlay, setVideoToPlay] = useState(null);
    return (
        <>

        {videoToPlay && displayVideoOverlay && (
        <section className="overlay">
          <div className="closeHitbox" onClick={closeVideoOverlay}></div>
          <div className="modal">
            <ReactPlayer className="video" playing={true} controls={true} volume={0} url={videoToPlay} />
          </div>
        </section>
      )}
        
        <div className="outline-section my-1 p-1">
        <h1 className="p-1"><b>Planificateur de raid</b></h1>

        <div className="grid">
            <div className="item no-link">
            <div className="p-1">
            <p className="label bold">- Sélection -</p>
            <small className="o-30 text-center">Sélectionnez le boss et l'élement de votre choix.</small>
            </div>
            <div className="d-flex">
                <div className={`item boss maxw-100`}>
                    <img className="bossImg interact" onClick={() => setDisplayBossSelection(true)} src={`../static/bosses/${selectedBoss}.png`}/>
                    <img className="icon element interact big" onClick={() => setDisplayElementSelection(true)} src={`../static/elements/${selectedElement}.svg`}/>
                </div>
            </div>
            </div>
            <div className="item no-link">
            <div className="p-1">
            <p className="label bold">- En focus -</p>
            <small className="o-30 text-center">Sélectionnez un des boss pour le pré-sélectionner instantanément.</small>
            </div>
                <div className="d-flex dir-column m-1">
                <div className={`item boss`} onClick={() => {setSelectedBoss("viper"); setSelectedElement("dark")}}>
                     <img className="bossImg" src={`../static/bosses/viper.png`}/>
                      <img className="icon element" src={`../static/elements/dark.svg`}/>
                  </div>
                  <div className={`item boss`} onClick={() => {setSelectedBoss("harvester"); setSelectedElement("light")}}>
                     <img className="" src={`../static/bosses/harvester.png`}/>
                      <img className="icon element" src={`../static/elements/light.svg`}/>
                  </div>
                <div className={`item boss`} onClick={() => {setSelectedBoss("gast"); setSelectedElement("water")}}>
                     <img className="" src={`../static/bosses/gast.png`}/>
                      <img className="icon element" src={`../static/elements/water.svg`}/>
                  </div>
                  <div className={`item boss`} onClick={() => {setSelectedBoss("slime"); setSelectedElement("fire")}}>
                     <img className="" src={`../static/bosses/slime.png`}/>
                      <img className="icon element" src={`../static/elements/fire.svg`}/>
                  </div>
                </div>
            </div>
        </div>
        </div>

        <div className="outline-section my-1 p-1">
        <h2 id="teams" className="title p-1">
            <b>Teams recommandées pour <span className="capitalize">{selectedBoss}</span></b>
            <img className="icon element iblock" src={`../static/elements/${selectedElement}.svg`}/>
        </h2>
        <div className="team-list">
        {Component && <Component onVideoPlay={handleVideoChange} />}
            </div>
        </div>

        {displayVideoOverlay && videoUrl && (
            <section className="overlay">
                <div className="closeHitbox" onClick={() => setDisplayVideoOverlay(false)}></div>
                <div className="modal">
                <ReactPlayer className="video" playing={true} controls={true} volume={0} url={videoUrl} />
                </div>
            </section>
        )}

        <div className={`overlay ${ displayElementSelection ? "" : "hidden"}`}>
            <div className="closeHitbox" onClick={() => setDisplayElementSelection(false)}></div>
            <div className={`modal elementModal`}>
                <div className="grid  m-0 ">
                    <div className={`item`} onClick={() => {setSelectedElement("light"); setDisplayElementSelection(false)}}>
                        <img className="icon element minw-50" src={`../static/elements/light.svg`}/>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedElement("dark"); setDisplayElementSelection(false)}}>
                        <img className="icon element minw-50" src={`../static/elements/dark.svg`}/>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedElement("basic"); setDisplayElementSelection(false)}}>
                        <img className="icon element minw-50" src={`../static/elements/basic.svg`}/>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedElement("earth"); setDisplayElementSelection(false)}}>
                        <img className="icon element minw-50" src={`../static/elements/earth.svg`}/>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedElement("fire"); setDisplayElementSelection(false)}}>
                        <img className="icon element minw-50" src={`../static/elements/fire.svg`}/>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedElement("water"); setDisplayElementSelection(false)}}>
                        <img className="icon element minw-50" src={`../static/elements/water.svg`}/>
                    </div>    
                </div>
            </div>
        </div>

        <div className={`overlay ${ displayBossSelection ? "" : "hidden"}`}>
            <div className="closeHitbox" onClick={() => setDisplayBossSelection(false)}></div>
            <div className={`modal bossModal`}>
                <div className="m-0 ">
                    <div className={`item`} onClick={() => {setSelectedBoss("commander"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/commander.png`}/>
                        <p className="name">Commandant</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("demon"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/demon.png`}/>
                        <p className="name">Démon</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("director"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/director.png`}/>
                        <p className="name">Réalisateur</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("duncan"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/duncan.png`}/>
                        <p className="name">Duncan</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("elphaba"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/elphaba.png`}/>
                        <p className="name">Elphaba</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("erina"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/erina.png`}/>
                        <p className="name">Erina</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("fairy"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/fairy.png`}/>
                        <p className="name">Fée</p>
                    </div>       
                    <div className={`item`} onClick={() => {setSelectedBoss("garam"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/garam.png`}/>
                        <p className="name">Garam</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("gast"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/gast.png`}/>
                        <p className="name">Gast</p>
                    </div>    
                    <div className={`item`} onClick={() => {setSelectedBoss("goblin"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/goblin.png`}/>
                        <p className="name">Chef Gobelin</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("harvester"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/harvester.png`}/>
                        <p className="name">Moissonneur</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("knight"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/knight.png`}/>
                        <p className="name">Chevalier de l'empire</p>
                    </div>
                    <div className={`item`} onClick={() => {setSelectedBoss("marina"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/marina.png`}/>
                        <p className="name">Marina</p>
                    </div>        
                    <div className={`item`} onClick={() => {setSelectedBoss("minotaur"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/minotaur.png`}/>
                        <p className="name">Minotaure</p>
                    </div>    
                    <div className={`item`} onClick={() => {setSelectedBoss("panda"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/panda.png`}/>
                        <p className="name">Panda Fou</p>
                    </div> 
                    <div className={`item`} onClick={() => {setSelectedBoss("shadow"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/shadow.png`}/>
                        <p className="name">Bête obscure</p>
                    </div>    
                    <div className={`item`} onClick={() => {setSelectedBoss("slime"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/slime.png`}/>
                        <p className="name">Boue de lave</p>
                    </div>    

                    <div className={`item`} onClick={() => {setSelectedBoss("viper"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/viper.png`}/>
                        <p className="name">Chef du clan Vipère</p>
                    </div>    
                    <div className={`item`} onClick={() => {setSelectedBoss("worm"); setDisplayBossSelection(false)}}>
                        <img className="boss" src={`../static/bosses/worm.png`}/>
                        <p className="name">Ver Taureau</p>
                    </div>    
                </div>
            </div>
        </div>
        </>
    );
}
