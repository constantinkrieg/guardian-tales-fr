import React from "react";
  
export default function Conquest() {

  return (
<div className="container">
  <ol className="even">
    <li className='hex'></li>
    <li className='hex'></li>
    <li className='hex'></li>
    <li className='hex'></li>
    <li className='hex'></li>
    <li className='hex'></li>
    <li className='hex'></li>
  </ol>  
  <ol className="odd">
    <li className='hex'></li>
    <li className='hex'></li>
    <li className='hex'></li>
  </ol>
  <ol className="even">
    <li className='hex'></li>
    <li className='hex'></li>
  </ol>  
</div>
  );
}