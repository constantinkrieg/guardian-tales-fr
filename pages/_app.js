import '../styles.scss'
import '../ui.scss'
import Script from 'next/script';

export default function Nextra({ Component, pageProps }) {
  return (
    <>
    <Script
     src="https://tools.luckyorange.com/core/lo.js?site-id=0ae2c22e"
      async
      defer />
    <Component {...pageProps} />
    </>
  )
} 