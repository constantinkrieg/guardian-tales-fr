// AOSInitializer.js
import { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

const AOSInitializer = () => {
  useEffect(() => {
    AOS.init({
      duration: 800,
      easing: 'ease-in-out',
      // ... other options
    });

    return () => {
      AOS.refresh();
    };
  }, []);

  return null; // This component doesn't render anything
};

export default AOSInitializer;