import React from 'react'
import { DocsThemeConfig } from 'nextra-theme-docs'
import Hotjar from '@hotjar/browser';

const siteId = 3717124;
const hotjarVersion = 6;

Hotjar.init(siteId, hotjarVersion);


const config: DocsThemeConfig = {
  nextThemes: {
    defaultTheme: 'dark'
  },
  useNextSeoProps() {
    return {
      titleTemplate: '%s – Guardian Tales TOP'
    }
  },
  head: (
    <>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta property="og:title" content="Guardian Tales TOP" />
      <title>Guardian Tales TOP</title>
      <meta name="keywords" content="Guardian, Tales, Top, Guide, FR, Français" />
      <meta name="description" content="- Actualités FR sur Guardian Tales" />
      <meta property="og:description" content="- Actualités FR sur Guardian Tales" />
      <link rel="apple-touch-icon" sizes="180x180" href="/static/favicon/apple-touch-icon.png"/>
      <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon/favicon-32x32.png"/>
      <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon/favicon-16x16.png"/>
      <link rel="manifest" href="/static/favicon/site.webmanifest"/>
      <meta name="msapplication-TileColor" content="#da532c"/>
      <meta name="theme-color" content="#ffffff"></meta>
    </>
  ),
  logo: <img className="logo" src="/static/logo.svg" alt="Guardian Tales TOP"/>,
  chat: {
    //link: 'https://discord.gg/4eVcDdgmGt',
  },
  footer: {
    text: '© 2023 Guardian Tales TOP Tous droits réservés. Toute représentation / reproduction intégrale ou partielle faite sans le consentement de l’auteur ou de ses ayant droit ou ayant cause est illicite. Guardian Tales TOP est un site web de fans pour le Action-RPG Guardian Tales développé par  Kakao Games Corp. et leurs associés. Guardian Tales TOP n’est pas affilié avec Kong Studio,  Kakao Games Corp. ou leurs associés. Tous les logos et dessins sont des marques commerciales et/ou déposées de Kong Studio, Kakao Games Corp. et de leurs associés. Toutes les autres marques commerciales appartiennent à leurs propriétaires respectifs.',
  },
  darkMode: false,
  search: {
    placeholder: "Rechercher... ",
  }
}

export default config
